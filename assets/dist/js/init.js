/** *************Init JS*********************
	
    TABLE OF CONTENTS
	---------------------------
	1.Ready function
	2.Load function
	3.Full height function
	4.grandin function
	5.Chat App function
	6.Resize function
 ** ***************************************/
 
var host = window.location.href.split('?')[0];
var host = host.split("/");
var host = host[0]+"//"+host[2]+"/"+host[3]+"/";

var tinggiX = $(window).innerHeight()-100+'px';
var tinggiY = $(window).innerHeight()-80+'px';
var tinggiZ = $(window).innerHeight()-215+'px';
	
 "use strict"; 
/*****Ready function start*****/
$(document).ready(function(){
	
	$('.areaGroup').css({ height: tinggiX });
	$('.areaGroupChat').css({ height: tinggiY });
	
	grandin();
	$('.preloader-it > .la-anim-1').addClass('la-animate');
});
/*****Ready function end*****/

/*****Load function start*****/
$(window).on("load",function(){
	$(".preloader-it").delay(500).fadeOut("slow");
	/*Progress Bar Animation*/
	var progressAnim = $('.progress-anim');
	if( progressAnim.length > 0 ){
		for(var i = 0; i < progressAnim.length; i++){
			var $this = $(progressAnim[i]);
			$this.waypoint(function() {
			var progressBar = $(".progress-anim .progress-bar");
			for(var i = 0; i < progressBar.length; i++){
				$this = $(progressBar[i]);
				$this.css("width", $this.attr("aria-valuenow") + "%");
			}
			}, {
			  triggerOnce: true,
			  offset: 'bottom-in-view'
			});
		}
	}
});
/*****Load function* end*****/

/***** Full height function start *****/
var setHeightWidth = function () {
	var height = $(window).height();
	var width = $(window).width();
	$('.full-height').css('height', (height));
	$('.page-wrapper').css('min-height', (height));
	
	/*Right Sidebar Scroll Start*/
	if(width<=1007){
		$('#chat_list_scroll').css('height', (height - 270));
		$('.fixed-sidebar-right .chat-content').css('height', (height - 279));
		$('.fixed-sidebar-right .set-height-wrap').css('height', (height - 219));
		
	}
	else {
		$('#chat_list_scroll').css('height', (height - 204));
		$('.fixed-sidebar-right .chat-content').css('height', (height - 213));
		$('.fixed-sidebar-right .set-height-wrap').css('height', (height - 153));
	}	
	/*Right Sidebar Scroll End*/
	
	/*Vertical Tab Height Cal Start*/
	var verticalTab = $(".vertical-tab");
	if( verticalTab.length > 0 ){ 
		for(var i = 0; i < verticalTab.length; i++){
			var $this =$(verticalTab[i]);
			$this.find('ul.nav').css(
			  'min-height', ''
			);
			$this.find('.tab-content').css(
			  'min-height', ''
			);
			height = $this.find('ul.ver-nav-tab').height();
			$this.find('ul.nav').css(
			  'min-height', height + 40
			);
			$this.find('.tab-content').css(
			  'min-height', height + 40
			);
		}
	}
	/*Vertical Tab Height Cal End*/
};
/***** Full height function end *****/

/***** grandin function start *****/
var $wrapper = $(".wrapper");
var grandin = function(){
	
	/*Counter Animation*/
	var counterAnim = $('.counter-anim');
	// if( counterAnim.length > 0 ){
		// counterAnim.counterUp({ delay: 10,
        // time: 1000});
	// }
	
	/*Tooltip*/
	if( $('[data-toggle="tooltip"]').length > 0 )
		$('[data-toggle="tooltip"]').tooltip();
	
	/*Popover*/
	if( $('[data-toggle="popover"]').length > 0 )
		$('[data-toggle="popover"]').popover()
	
	
	/*Sidebar Collapse Animation*/
	var sidebarNavCollapse = $('.fixed-sidebar-left .side-nav  li .collapse');
	var sidebarNavAnchor = '.fixed-sidebar-left .side-nav  li a';
	$(document).on("click",sidebarNavAnchor,function (e) {
		if ($(this).attr('aria-expanded') === "false")
				$(this).blur();
		$(sidebarNavCollapse).not($(this).parent().parent()).collapse('hide');
	});
	
	/*Panel Remove*/
	$(document).on('click', '.close-panel', function (e) {
		var effect = $(this).data('effect');
			$(this).closest('.panel')[effect]();
		return false;	
	});
	
	/*Accordion js*/
		$(document).on('show.bs.collapse', '.panel-collapse', function (e) {
		$(this).siblings('.panel-heading').addClass('activestate');
	});
	
	$(document).on('hide.bs.collapse', '.panel-collapse', function (e) {
		$(this).siblings('.panel-heading').removeClass('activestate');
	});
	
	/*Sidebar Navigation*/
	$(document).on('click', '#toggle_nav_btn,#open_right_sidebar,#setting_panel_btn', function (e) {
		$(".dropdown.open > .dropdown-toggle").dropdown("toggle");
		return false;
	});
	$(document).on('click', '#toggle_nav_btn', function (e) {
		$wrapper.removeClass('open-right-sidebar open-setting-panel').toggleClass('slide-nav-toggle');
		return false;
	});
	
	$(document).on('click', '#open_right_sidebar', function (e) {
		$wrapper.toggleClass('open-right-sidebar').removeClass('open-setting-panel');
		return false;
	
	});
	
	$(document).on('click','.product-carousel .owl-nav',function(e){
		return false;
	});
	
	$(document).on('click', 'body', function (e) {
		if($(e.target).closest('.fixed-sidebar-right,.setting-panel').length > 0) {
			return;
		}
		$('body > .wrapper').removeClass('open-right-sidebar open-setting-panel');
		return;
	});
	
	$(document).on('show.bs.dropdown', '.nav.navbar-right.top-nav .dropdown', function (e) {
		$wrapper.removeClass('open-right-sidebar open-setting-panel');
		return;
	});
	
	$(document).on('click', '#setting_panel_btn', function (e) {
		$wrapper.toggleClass('open-setting-panel').removeClass('open-right-sidebar');
		return false;
	});
	$(document).on('click', '#toggle_mobile_nav', function (e) {
		$wrapper.toggleClass('mobile-nav-open').removeClass('open-right-sidebar');
		return;
	});
	

	$(document).on("mouseenter mouseleave",".wrapper > .fixed-sidebar-left", function(e) {
		if (e.type == "mouseenter") {
			$wrapper.addClass("sidebar-hover"); 
		}
		else { 
			$wrapper.removeClass("sidebar-hover");  
		}
		return false;
	});
	
	$(document).on("mouseenter mouseleave",".wrapper > .setting-panel", function(e) {
		if (e.type == "mouseenter") {
			$wrapper.addClass("no-transition"); 
		}
		else { 
			$wrapper.removeClass("no-transition");  
		}
		return false;
	});
	
	/*Todo*/
	var random = Math.random();
	$(document).on("keypress","#add_todo",function (e) {
		if ((e.which == 13)&&(!$(this).val().length == 0))  {
				$('<li class="todo-item"><div class="checkbox checkbox-success"><input type="checkbox" id="checkbox'+random+'"/><label for="checkbox'+random+'">' + $('.new-todo input').val() + '</label></div></li><li><hr class="light-grey-hr"/></li>').insertAfter(".todo-list li:last-child");
				$('.new-todo input').val('');
		} else if(e.which == 13) {
			alert('Please type somthing!');
		}
		return;
	});
	
	/*Chat*/
	$(document).on("keypress","#input_msg_send",function (e) {
		if ((e.which == 13)&&(!$(this).val().length == 0)) {
			$('<li class="self mb-10"><div class="self-msg-wrap"><div class="msg block pull-right">' + $(this).val() + '<div class="msg-per-detail mt-5"><span class="msg-time txt-grey">3:30 pm</span></div></div></div><div class="clearfix"></div></li>').insertAfter(".fixed-sidebar-right .chat-content  ul li:last-child");
			$(this).val('');
		} else if(e.which == 13) {
			alert('Please type somthing!');
		}
		return;
	});
	$(document).on("keypress","#input_msg_send_widget",function (e) {
		if ((e.which == 13)&&(!$(this).val().length == 0)) {
			$('<li class="self mb-10"><div class="self-msg-wrap"><div class="msg block pull-right">' + $(this).val() + '<div class="msg-per-detail mt-5"><span class="msg-time txt-grey">3:30 pm</span></div></div></div><div class="clearfix"></div></li>').insertAfter(".chat-for-widgets .chat-content  ul li:last-child");
			$(this).val('');
		} else if(e.which == 13) {
			alert('Please type somthing!');
		}
		return;
	});
	$(document).on("keypress","#input_msg_send_chatapp",function (e) {
		if ((e.which == 13)&&(!$(this).val().length == 0)) {
			var msg 		= $(this).val();
			var idReceiver 	= $('#idReceiver').val();
			// var groupType 	= $('#idGroupType').val();
			$.post(host+'main/send_chat',{idReceiver:idReceiver,msg:msg}).done(function(data){
				console.log(data);return false;
			})
			// $('<li class="self mb-10"><div class="self-msg-wrap"><div class="msg block pull-right">' + $(this).val() + '<div class="msg-per-detail mt-5"><span class="msg-time txt-grey">3:30 pm</span></div></div></div><div class="clearfix"></div></li>').insertAfter(".chat-for-widgets-1 .chat-content  ul li:last-child");
			$(this).val('');
		} else if(e.which == 13) {
			alert('Please type a something!');
		}
		return;
	});
	$(document).on("click","#button_msg_send_chatapp",function (e) {
		var msg = $('#input_msg_send_chatapp').val();
		if (msg!='') {
			
			var idReceiver 	= $('#idReceiver').val();
			// var groupType 	= $('#idGroupType').val();
			$.post(host+'main/send_chat',{idReceiver:idReceiver,msg:msg});
			
			$('#input_msg_send_chatapp').val('');
		} else{
			alert('Please type a something!');
		}
		return;
	});
	$('#msg_attachment').on('change',function(){
		var idReceiver 	= $('#idReceiver').val();
		var file_data 	= $('#msg_attachment').prop('files')[0];
		var form_data 	= new FormData();
		form_data.append('file', file_data);
		form_data.append('idReceiver', idReceiver);
		$.ajax({
			url: host+'main/send_attachment', // point to server-side controller method
			dataType: 'text', // what to expect back from the server
			cache: false,
			contentType: false,
			processData: false,
			data: form_data,
			type: 'post',
			success: function (response) {
				console.log(response);
				// demo.showNotification('bottom','left','success','Ubah Background Login Admin Berhasil!');
				// setTimeout(function(){ location.reload(); }, 1500);
			},
			error: function (response) {
				// console.log(response);
			}
		});
	})
	$(document).on("click",".fixed-sidebar-right .chat-cmplt-wrap .chat-data",function (e) {
		$(".fixed-sidebar-right .chat-cmplt-wrap").addClass('chat-box-slide');
		return false;
	});
	$(document).on("click",".fixed-sidebar-right #goto_back",function (e) {
		$(".fixed-sidebar-right .chat-cmplt-wrap").removeClass('chat-box-slide');
		return false;
	});
	
	/*Chat for Widgets*/
	$(document).on("click",".chat-for-widgets.chat-cmplt-wrap .chat-data",function (e) {
		$(".chat-for-widgets.chat-cmplt-wrap").addClass('chat-box-slide');
		return false;
	});
	$(document).on("click","#goto_back_widget",function (e) {
		$(".chat-for-widgets.chat-cmplt-wrap").removeClass('chat-box-slide');
		return false;
	});
	/*Horizontal Nav*/
	$(document).on("show.bs.collapse",".top-fixed-nav .fixed-sidebar-left .side-nav > li > ul",function (e) {
		e.preventDefault();
	});
	
	/*Slimscroll*/
	$('.nicescroll-bar').slimscroll({height:'100%',color: '#878787', disableFadeOut : true,borderRadius:0,size:'4px',alwaysVisible:false});
	$('.message-nicescroll-bar').slimscroll({height:'229px',size: '4px',color: '#878787',disableFadeOut : true,borderRadius:0});
	$('.message-box-nicescroll-bar').slimscroll({height:'350px',size: '4px',color: '#878787',disableFadeOut : true,borderRadius:0});
	$('.product-nicescroll-bar').slimscroll({height:'346px',size: '4px',color: '#878787',disableFadeOut : true,borderRadius:0});
	$('.app-nicescroll-bar').slimscroll({height:'162px',size: '4px',color: '#878787',disableFadeOut : true,borderRadius:0});
	$('.todo-box-nicescroll-bar').slimscroll({height:'285px',size: '4px',color: '#878787',disableFadeOut : true,borderRadius:0});
	$('.users-nicescroll-bar').slimscroll({height:'370px',size: '4px',color: '#878787',disableFadeOut : true,borderRadius:0});
	$('.users-chat-nicescroll-bar').slimscroll({height:'257px',size: '4px',color: '#878787',disableFadeOut : true,borderRadius:0});
	$('.chatapp-nicescroll-bar').slimscroll({height:'543px',size: '4px',color: '#878787',disableFadeOut : true,borderRadius:0});
	$('.chatapp-chat-nicescroll-bar').slimscroll({height:tinggiZ,size: '4px',color: '#878787',disableFadeOut : true,borderRadius:0});
	
	/*Product carousel*/
	if( $('.product-carousel').length > 0 )
	var $owl = $('.product-carousel').owlCarousel({
		loop:true,
		margin:15,
		nav:true,
		navText: ["<i class='zmdi zmdi-chevron-left'></i>","<i class='zmdi zmdi-chevron-right'></i>"],
		dots:false,
		autoplay:true,
		responsive:{
			0:{
				items:1
			},
			400:{
				items:2
			},
			767:{
				items:3
				},
			1399:{
				items:4
			}
		}
	});
	
	/*Refresh Init Js*/
	var refreshMe = '.refresh';
	$(document).on("click",refreshMe,function (e) {
		var panelToRefresh = $(this).closest('.panel').find('.refresh-container');
		var dataToRefresh = $(this).closest('.panel').find('.panel-wrapper');
		var loadingAnim = panelToRefresh.find('.la-anim-1');
		panelToRefresh.show();
		setTimeout(function(){
			loadingAnim.addClass('la-animate');
		},100);
		function started(){} //function before timeout
		setTimeout(function(){
			function completed(){} //function after timeout
			panelToRefresh.fadeOut(800);
			setTimeout(function(){
				loadingAnim.removeClass('la-animate');
			},800);
		},1500);
		  return false;
	});
	
	/*Fullscreen Init Js*/
	$(document).on("click",".full-screen",function (e) {
		$(this).parents('.panel').toggleClass('fullscreen');
		$(window).trigger('resize');
		return false;
	});
	
	/*Nav Tab Responsive Js*/
	$(document).on('show.bs.tab', '.nav-tabs-responsive [data-toggle="tab"]', function(e) {
		var $target = $(e.target);
		var $tabs = $target.closest('.nav-tabs-responsive');
		var $current = $target.closest('li');
		var $parent = $current.closest('li.dropdown');
			$current = $parent.length > 0 ? $parent : $current;
		var $next = $current.next();
		var $prev = $current.prev();
		$tabs.find('>li').removeClass('next prev');
		$prev.addClass('prev');
		$next.addClass('next');
		return;
	});
};
/***** grandin function end *****/

/***** Chat App function Start *****/
var chatAppTarget = $('.chat-for-widgets-1.chat-cmplt-wrap');
var chatApp = function() {
	$(document).on("click",".chat-for-widgets-1.chat-cmplt-wrap .chat-data",function (e) {
		var width = $(window).width();
		if(width<=1007) {
			chatAppTarget.addClass('chat-box-slide');
		}
		return false;
	});
	$(document).on("click","#goto_back_widget_1",function (e) {
		var width = $(window).width();
		if(width<=1007) {
			chatAppTarget.removeClass('chat-box-slide');
		}	
		return false;
	});
};
/***** Chat App function End *****/

var boxLayout = function() {
	if((!$wrapper.hasClass("rtl-layout"))&&($wrapper.hasClass("box-layout")))
		$(".box-layout .fixed-sidebar-right").css({right: $wrapper.offset().left + 300});
		else if($wrapper.hasClass("box-layout rtl-layout"))
			$(".box-layout .fixed-sidebar-right").css({left: $wrapper.offset().left});
}
boxLayout();	

/**Only For Setting Panel Start**/

/*Fixed Slidebar*/
var fixedHeader = function() {
	if($(".setting-panel #switch_3").is(":checked")) {
		$wrapper.addClass("scrollable-nav");
	} else {
		$wrapper.removeClass("scrollable-nav");
	}
};
fixedHeader();	
$(document).on('change', '.setting-panel #switch_3', function () {
	fixedHeader();
	return false;
});	

/*Theme Color Init*/
$(document).on('click', '.theme-option-wrap > li', function (e) {
	$(this).addClass('active-theme').siblings().removeClass('active-theme');
	$wrapper.removeClass (function (index, className) {
		return (className.match (/(^|\s)theme-\S+/g) || []).join(' ');
	}).addClass($(this).attr('id')+'-active');
	return false;	
});

/*Primary Color Init*/
var primaryColor = 'input:radio[name="radio-primary-color"]';
if( $('input:radio[name="radio-primary-color"]').length > 0 ){
	//$(primaryColor)[0].checked = true;
	$(document).on('click',primaryColor, function (e) {
		$wrapper.removeClass (function (index, className) {
			return (className.match (/(^|\s)pimary-color-\S+/g) || []).join(' ');
		}).addClass($(this).attr('id'));
		return;
	});
}
$('#idInformation').click(function(){
	if($.browser.android){
		var msg = '<button onclick="joinMeeting(\'6674241347\')" class="btn btn-xs btn-primary">Join Meeting</button>';
		var idReceiver 	= $('#idReceiver').val();
		// var groupType 	= $('#idGroupType').val();
		Android.createMeeting();
		setTimeout(function(){
			$.post(host+'main/send_chat',{idReceiver:idReceiver,msg:msg});
		}, 2000); 
	}else{
		alert("Fitur Hanya Berjalan Pada Smartphone Android!");
	}
})
function joinMeeting(idMeeting){
	Android.joinMeeting(idMeeting,'Joko');
}
function update_token(username,token){
	$.post(host+'main/update_token',{username:username,token:token});
}
function showAndroidToast(toast) {
	// Android.showToast(toast);
	Android.previewImage();
}
/* SCROLL DOWN GW */
function scrollDown(){
	var t = $('.chatapp-chat-nicescroll-bar');
    t.animate({"scrollTop": $('.chatapp-chat-nicescroll-bar')[0].scrollHeight}, "slow");
}

function downloadImage(pathFile,id,pathFileThumbnail=null){
	// Android.previewImage();
	
	$.post(host+'main/download_image',{id:id}).done(function(data){
		var link = document.createElement('a');
		link.href = pathFile;
		link.download = '';
		document.body.appendChild(link);
		link.click();
		document.body.removeChild(link);
		
		//Hapus img-holder
		var imgHolder = $('#id-imgHolder-'+id).contents();
		$("#id-imgHolder-"+id).replaceWith(imgHolder);
		
		//Hapus Overlay Gambar
		var overlayPreviewGambar = $('#idOverlayPreviewGambar-'+id).contents();
		$("#idOverlayPreviewGambar-"+id).replaceWith(overlayPreviewGambar);
		
		// Android.previewImage();
		
		// Hapus Button
		$('#btnDownloadImage-'+id).remove();
		
		//Update Gambar Ori
		if(pathFileThumbnail==null||pathFileThumbnail==''){
			$('#idImagePreview-'+id).attr('src',pathFile);
		}else{
			$('#idImagePreview-'+id).attr('src',pathFileThumbnail);
		}
		
	})
}

function previewImage(filename){
	Android.previewImage(filename);
}

function previewVideo(filename){
	Android.previewVideo(filename);
}

/*Reset Init*/
$(document).on('click', '#reset_setting', function (e) {
	$('.theme-option-wrap > li').removeClass('active-theme').first().addClass('active-theme');
	$wrapper.removeClass (function (index, className) {
		return (className.match (/(^|\s)theme-\S+/g) || []).join(' ');
	}).addClass('theme-1-active');
	if($(".setting-panel #switch_3").is(":checked"))
		$('.setting-panel .layout-switcher .switchery').trigger('click');
		$('#pimary-color-green').trigger('click');
	return false;	
});

	
/*Switchery Init*/
var elems = Array.prototype.slice.call(document.querySelectorAll('.setting-panel .js-switch'));
$('.setting-panel .js-switch').each(function() {
	new Switchery($(this)[0], $(this).data());
});

/*Only For Setting Panel end*/

/***** Resize function start *****/
$(window).on("resize", function () {
	setHeightWidth();
	boxLayout();
	chatApp();
}).resize();
/***** Resize function end *****/


$(".register_direktorat").change(function(){
	alert('ea');
})

var tempAdministratorAccess=1;

$(".ubahPengguna").click(function(){
	var id = this.id;
	$.post(host+'master/get_pengguna',{id:id}).done(function(data){
		var obj = jQuery.parseJSON(data);
		
		if(obj['last_name']=='' || obj['last_name']==null){
			$("#idNamaLengkap").val(obj['first_name']);
		}else{
			$("#idNamaLengkap").val(obj['first_name']+obj['last_name']);
		}
		
		$("#idTelepon").val(obj['phone']);
		
		$("#idCurrentUser").val(obj['id']);
		
		// console.log(obj);
		
		if(obj['level']==1){
			$('.bs-switch').bootstrapSwitch('state', true);
		}else{
			$('.bs-switch').bootstrapSwitch('state', false);
			tempAdministratorAccess++;
		}
	})
})

$(".ubahGrup").click(function(){
	var id = this.id;
	$.post(host+'master/get_grup',{id:id}).done(function(data){
		var obj = jQuery.parseJSON(data);
		$("#idMaster_IdGrup").val(obj['id']);
		$("#idMaster_NamaGrup").val(obj['name']);
		$("#idMaster_unit option[value="+obj['unit']+"]").attr('selected','selected');
	})
})

$(".aturAnggota").click(function(){
	var id = this.id;
	
	$("#idModalTambahAnggota_group").val(id);
	
	//clear
	$("#idMasterGrup_tbody").html('');
			
	$.post(host+'master/get_anggota_grup',{id:id}).done(function(data){
		if(data == 0){
			
		}else{
			var obj = jQuery.parseJSON(data);
			
			for(var x=0;x<obj.length;x++){
				
				var temp_pp=null;
				if(obj[x]['pp']=='' || obj[x]['pp'] == null){
					temp_pp = host+'assets/img/default-avatar.png';
				}else{
					temp_pp = host+'uploads/images/pp/'.obj[x]['pp'];
				}
				//reinitialize
				$("#idMasterGrup_tbody").append('<tr id="idMasterGrup_tbody_'+obj[x]['id']+'"><td width="20%"><img width="100%" class="img-circle" style="vertical-align:middle;" src="'+temp_pp+'"></td><td>'
												+obj[x]['name']
												+'</td><td>'+
												'<button style="max-width:30px;max-height:30px;" onclick="hapusAnggotaGrup('+obj[x]['id']+')" class="btn btn-danger btn-icon-anim btn-circle btn-xs pull-right"><i class="fa fa-trash"></i></button></td></tr>');
			}
			
			$("#footable_22").footable();
		}
	})
})

$("#btnTambahAnggota").click(function(){
	var id = $("#idModalTambahAnggota_group").val();
	$("#idMasterGrupTambahAnggota_tbody").html('');
	
	$.post(host+'master/get_all_anggota_grup_where',{id:id}).done(function(data){
		var obj = jQuery.parseJSON(data);
		var obj_all_anggota = obj['all_anggota'];
		
			for(var x=0;x<obj_all_anggota.length;x++){
				
				var temp_pp=null;
				if(obj_all_anggota[x]['pp']=='' || obj_all_anggota[x]['pp'] == null){
					temp_pp = host+'assets/img/default-avatar.png';
				}else{
					temp_pp = host+'uploads/images/pp/'.obj_all_anggota[x]['pp'];
				}
				//reinitialize
				$("#idMasterGrupTambahAnggota_tbody").append('<tr><td width="20%"><img width="100%" class="img-circle" style="vertical-align:middle;" src="'+temp_pp+'"></td><td>'
												+obj_all_anggota[x]['first_name']
												+'</td><td>'+
												'<button style="max-width:30px;max-height:30px;" onclick="tambahAnggotaGrup('+obj_all_anggota[x]['id']+')" class="btn btn-danger btn-icon-anim btn-circle btn-xs pull-right"><i class="fa fa-chevron-right"></i></button></td></tr>');
			}
			
			$("#footable_228").footable();
			
	})
	
	$("#idBody01").addClass("hide");
	$("#idBody02").removeClass("hide");
})

function tambahAnggotaGrup(idUser){
	var idGroup = $("#idModalTambahAnggota_group").val();
	var x = confirm("Apakah anda yakin?");
	if(x){
		$.post(host+'master/tambahAnggotaGrup',{idUser:idUser,idGroup:idGroup}).done(function(data){
			console.log(data);
		})
	}
}

function hapusAnggotaGrup(idUser){
	var idGroup = $("#idModalTambahAnggota_group").val();
	var x = confirm("Apakah anda yakin?");
	if(x){
		$.post(host+'master/hapusAnggotaGrup',{idUser:idUser,idGroup:idGroup}).done(function(data){
			$("#idMasterGrup_tbody_"+idUser).remove();
		})
	}
}

$("#btnKembaliTambahAnggota").click(function(){
	$("#idBody02").addClass("hide");
	$("#idBody01").removeClass("hide");
})

$(".ubahFAQ").click(function(){
	var id = this.id;
	$.post(host+'master/get_faq',{id:id}).done(function(data){
		var obj = jQuery.parseJSON(data);
		
		$("#idFaq_id").val(obj['id']);
		$("#idFaq_judul").val(obj['judul']);
		$("#idFaq_deskripsi").val(obj['deskripsi']);
		
		// $("#idCurrentUser").val(obj['id']);
		
	})
})

$(".hapusFAQ").click(function(){
	var id = this.id;
	var x = confirm("Apakah anda yakin?");
	if(x){
		$.post(host+'master/del_faq',{id:id}).done(function(data){
			// swal("Proses Berhasil", "Hapus FAQ Berhasil Dilakukan", "success");
			location.reload();
		})
	}
})


$('#check_box_switch').on('switchChange.bootstrapSwitch', function () {
	if(tempAdministratorAccess!=1){
		var state = $('#check_box_switch').bootstrapSwitch('state');
		var id = $("#idCurrentUser").val();
		if(state){
			$.post(host+'master/upd_pengguna',{id:id,state:true});
		}else{
			$.post(host+'master/upd_pengguna',{id:id,state:false});
		}
		// swal("Proses Berhasil", "Perubahan Berhasil Dilakukan", "success");
	}else{
		tempAdministratorAccess++;
	}
});
