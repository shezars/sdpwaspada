<!-- Main Content -->
<div class="page-wrapper">
	<div class="container-fluid">
		
		<!-- Row -->
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default card-view">
					<div class="panel-wrapper collapse in">
						<div class="panel-body pa-15">
							<div class="panel-group accordion-struct"  role="tablist" aria-multiselectable="true">
								<?php
								$x=1;
								foreach($faq as $row){
									?>
									<div class="panel panel-default">
										<div class="panel-heading" role="tab" id="headingFive">
											<a role="button" data-toggle="collapse" href="#collapseFive_<?=$x;?>" aria-expanded="true" aria-controls="collapseFive" ><?=$row['judul'];?></a> 
										</div>
										<div id="collapseFive_<?=$x;?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
											<div class="panel-body pa-15"><?=$row['deskripsi'];?></div>
										</div>
									</div>
									<?php
								$x++;
								}
								?>
							</div>
						</div>
					</div>
				</div>
		</div>
		</div>
		<!-- /Row -->
		
	</div>
	<!-- Footer -->
	<footer class="footer container-fluid pl-30 pr-30">
		<div class="row">
			<div class="col-sm-12">
				<p>2018 &copy; Grandin. Pampered by Hencework</p>
			</div>
		</div>
	</footer>
	<!-- /Footer -->
</div>