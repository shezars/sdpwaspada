<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<title>SDP Waspada</title>
		<meta name="description" content="Grandin is a Dashboard & Admin Site Responsive Template by hencework." />
		<meta name="keywords" content="admin, admin dashboard, admin template, cms, crm, Grandin Admin, Grandinadmin, premium admin templates, responsive admin, sass, panel, software, ui, visualization, web app, application" />
		<meta name="author" content="hencework"/>
		
		<!-- Favicon -->
		<link rel="shortcut icon" href="favicon.ico">
		<link rel="icon" href="favicon.ico" type="image/x-icon">
		
		<!-- vector map CSS -->
		<!--
		<link href="<?=base_url();?>assets/vendors/bower_components/jasny-bootstrap/dist/css/jasny-bootstrap.min.css" rel="stylesheet" type="text/css"/>
		-->
		
		
		<!-- Custom CSS -->
		<link href="<?=base_url();?>assets/dist/css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<!--Preloader-->
		<div class="preloader-it">
			<div class="la-anim-1"></div>
		</div>
		<!--/Preloader-->
		
		<div class="wrapper pa-0">
			<header class="sp-header">
				<div class="sp-logo-wrap" style="text-align:center;">
					<a href="index.html">
						<img class="brand-img mr-10" src="<?=base_url();?>assets/img/logo.png" alt="brand"/>
					</a>
				</div>
				<div class="clearfix"></div>
			</header>
			
			<!-- Main Content -->
			<div class="page-wrapper pa-0 ma-0 auth-page">
				<div class="container-fluid">
					<!-- Row -->
					<div class="table-struct full-width full-height">
						<div class="table-cell vertical-align-middle auth-form-wrap">
							<div class="auth-form  ml-auto mr-auto no-float card-view pt-30 pb-30">
								<div class="row">
									<div class="col-sm-12 col-xs-12">
										<div class="mb-30">
											<h3 class="text-center txt-dark mb-10">Sign in</h3>
											<h6 class="text-center nonecase-font txt-grey">Enter your details below</h6>
										</div>	
										<div class="form-wrap">
											<!--
											<form action="<?=base_url();?>login/do_login" method="POST">
											-->
												<div class="form-group">
													<label class="control-label mb-10" for="idUsername">NIP</label>
													<input type="text" name="username" class="form-control" required="" id="idUsername" placeholder="NIP">
												</div>
												<div class="form-group">
													<label class="pull-left control-label mb-10" for="idPassword">Password</label>
													<div class="clearfix"></div>
													<input type="password" name="password" class="form-control" required="" id="idPassword" placeholder="Password">
												</div>
												<div class="form-group text-center">
													<button type="button" class="btn btn-block btn-primary  btn-rounded" id="btnSignIn">sign in</button>
													<a href="<?=base_url();?>register" class="btn btn-block btn-warning  btn-rounded">sign up</a>
													<a href="<?=base_url();?>register/forgot_password" class="btn btn-block btn-danger btn-rounded">Forgot Password</a>
												</div>
												<div id="idAreaNotifikasi"></div>
											<!--
											</form>
											-->
										</div>
									</div>	
								</div>
							</div>
						</div>
					</div>
					<!-- /Row -->	
				</div>
				
			</div>
			<!-- /Main Content -->
		
		</div>
		<!-- /#wrapper -->
		
		<!-- JavaScript -->
		
		<!-- jQuery -->
		<script src="<?=base_url();?>assets/vendors/bower_components/jquery/dist/jquery.min.js"></script>
		
		<!-- Bootstrap Core JavaScript -->
		<script src="<?=base_url();?>assets/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
		<!--
			<script src="<?=base_url();?>assets/vendors/bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js"></script>
		-->
		
		<!-- Slimscroll JavaScript -->
		<script src="<?=base_url();?>assets/dist/js/jquery.slimscroll.js"></script>

		<!-- JQuery Browser -->
		<script src="<?=base_url();?>assets/dist/js/jquery.browser.js"></script>
		
		<script>
		$("#btnSignIn").click(function(data){
			var username = $("#idUsername").val();
			var password = $("#idPassword").val();
			$.post('<?=base_url();?>login/do_login',{username:username,password:password}).done(function(data){
				// console.log(data);return false;
				var obj = jQuery.parseJSON(data);
				if(obj.status == 1){
					
					if($.browser.android){
						// Login Zoom Android Only
						// Android.showToast('Indonesia Raya OKE');
						
						// Android.loginZoom('shezars@gmail.com','*123QWEasd');
						// Android.getToken(username);
					}
					window.location.href = "<?=base_url();?>";
				}else if(obj.status == 99){
					//Belum Approve
					$("#idAreaNotifikasi").html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+obj.info+'</div>');
				}else{
					//Username atau Password Tidak Sesuai
					$("#idAreaNotifikasi").html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+obj.info+'</div>');
				}
			})
		})
		</script>
		<!-- Init JavaScript -->
		<script src="<?=base_url();?>assets/dist/js/init.js"></script>
	</body>
</html>
