<!-- Main Content -->
<div class="page-wrapper">
	<div class="container-fluid pt-25">
			
		<!-- Row -->
		<div class="row">
			<div class="col-lg-3 col-xs-12">
				<div class="panel panel-default card-view  pa-0">
					<div class="panel-wrapper collapse in">
						<div class="panel-body  pa-0">
							<div class="profile-box">
								<div class="profile-cover-pic">
									<div class="profile-image-overlay"></div>
								</div>
								<div class="profile-info text-center">
									<div class="profile-img-wrap">
										<img class="inline-block mb-10" id="idPP_Profile" src="<?=$data->pp==''?base_url().'assets/img/mock1.jpg':base_url().'uploads/images/pp/'.$data->pp;?>" alt="user"/>
										<form enctype="multipart/form-data" id="infoForm1">
										<div class="fileupload btn btn-default">
											<span class="btn-text">edit</span>
											<input name="foto" class="upload" type="file">
										</div>
										</form>
									</div>	
									<h5 class="block mt-10 mb-5 weight-500 capitalize-font" id="idProfile_complete_name"><?=$this->ion_auth->user()->row()->first_name.$this->ion_auth->user()->row()->last_name;?></h5>
									<h6 class="block capitalize-font pb-20" id="idProfile_description"><?=$this->ion_auth->get_users_groups($this->ion_auth->get_user_id())->row()->description;?></h6>
								</div>	
								<div class="social-info">
									<div class="row">
										<div class="col-xs-12 text-center">
											<span class="counts block head-font"><span class="counter-anim" id="idProfile_jabatan"><?=$data->nm_jabatan;?></span></span>
											<span class="counts-text block">Jabatan</span>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 text-center">
											<span class="counts block head-font"><span class="counter-anim" id="idProfile_telp"><?=$data->no_hp;?></span></span>
											<span class="counts-text block">Telepon</span>
										</div>
									</div>
									<div class="row" align="center">
										<button class="btn btn-primary btn-anim mt-30 editProfile" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil"></i><span class="btn-text">edit profile</span></button>
										<button class="btn btn-primary btn-anim mt-30 editAccount" data-toggle="modal" data-target="#myModal2"><i class="fa fa-pencil"></i><span class="btn-text">edit account</span></button>
									</div>
									<div id="myModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
													<h5 class="modal-title" id="myModalLabel">Edit Profile</h5>
												</div>
												<div class="modal-body" id="idModalBody_ubahprofile">
													<!-- Row -->
													<div class="row">
														<div class="col-lg-12">
															<div class="">
																<div class="panel-wrapper collapse in">
																	<div class="panel-body pa-0">
																		<div class="col-sm-12 col-xs-12">
																			<div class="form-wrap">
																				<form action="<?=base_url();?>profil/update_profile" method="POST">
																					<div class="form-body overflow-hide">
																						<div class="form-group">
																							<label class="control-label mb-10" for="profil_idNamaLengkap">Nama Lengkap</label>
																							<div class="input-group">
																								<div class="input-group-addon"><i class="icon-user"></i></div>
																								<input type="text" class="form-control" id="profil_idNamaLengkap" placeholder="Masukkan Nama Lengkap">
																							</div>
																						</div>
																						<div class="form-group">
																							<label class="control-label mb-10" for="profil_idJabatan">Jabatan <button class="btn btn-xs btn-primary" id="idBtnModal_ubahjabatan" style="padding:4px 10px;"><i class="fa fa-exchange"></i></button></label>
																							<div class="input-group">
																								<div class="input-group-addon"><i class="icon-user"></i></div>
																								<input type="text" disabled class="form-control" id="profil_idJabatan" placeholder="Masukkan Jabatan">
																							</div>
																						</div>
																						<!--
																						<div class="form-group">
																							<label class="control-label mb-10" for="exampleInputEmail_1">Email address</label>
																							<div class="input-group">
																								<div class="input-group-addon"><i class="icon-envelope-open"></i></div>
																								<input type="email" class="form-control" id="exampleInputEmail_1" placeholder="xyz@gmail.com">
																							</div>
																						</div>-->
																						<div class="form-group">
																							<label class="control-label mb-10" for="profil_idTelepon">Telepon</label>
																							<div class="input-group">
																								<div class="input-group-addon"><i class="icon-phone"></i></div>
																								<input type="email" class="form-control" id="profil_idTelepon" placeholder="Masukkan Nomor Telepon">
																							</div>
																						</div>
																					</div>			
																				</form>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="modal-body hide" id="idModalBody_ubahjabatan">
													<!-- Row -->
													<div class="row">
														<div class="col-lg-12">
															<div class="">
																<div class="panel-wrapper collapse in">
																	<div class="panel-body pa-0">
																		<div class="col-sm-12 col-xs-12">
																			<div class="form-wrap">
																				<form action="<?=base_url();?>profil/update_profile" method="POST">
																					<div class="form-body overflow-hide">
																						<div class="form-group">
																							<div class="input-group">
																								<div class="input-group-addon">UNIT</div>
																								<!--
																								<input type="text" class="form-control" id="profil_idNamaLengkap" placeholder="Masukkan Nama Lengkap">
																								-->
																								<select class="form-control pilihUnit">
																									<option value=0>-- pilih unit --</option>
																									<option value=1>Ditjenpas</option>
																									<option value=2>Kanwil</option>
																									<option value=3>UPT</option>
																								</select>
																							</div>
																							<div class="ditjenpasArea hide">
																								<div class="form-group">
																									<label class="control-label mb-10">&nbsp;</label>
																									<select class="form-control" id="idDirektorat">
																										<option selected="true" disabled="disabled">-- pilih direktorat --</option>
																										<?php
																										foreach($direktorat as $row){
																											?>
																											<option value="<?=$row['id_struktur'];?>"><?=$row['nm_struktur'];?></option>
																											<?php
																										}
																										?>
																									</select>
																								</div>
																								<div class="form-group">
																									<label>Pilih Subdirektorat</label><br>
																									<select id="idSubDirektorat" class="form-control"></select>
																								</div>
																								<div class="form-group">
																									<label>Pilih Jabatan</label><br>
																									<select id="idJabatan" class="form-control"></select>
																								</div>
																							</div>
																							<div class="kanwilArea hide">
																								<div class="form-group">
																									<label>Kanwil</label><br>
																									<div class="typeahead__container">
																										<div class="typeahead__field">
																											<div class="typeahead__query">
																												<input class="js-typeahead-car_v1" id="idKanwil_Register" name="car_v1[query]" type="search" placeholder="Search" autocomplete="off">
																											</div>
																										</div>
																									</div>	                                            
																								</div>
																								<div class="form-group">
																									<label>Pilih Jabatan</label><br>
																									<select id="idKanwilJabatan" class="form-control">
																									<option selected="true" disabled="disabled">-- pilih jabatan --</option>
																									<?php
																									foreach($kanwil_jabatan as $row){
																										?>
																										<option value="<?=$row['id_struktur_jabatan'];?>"><?=$row['nm_jabatan'];?></option>
																										<?php
																									}
																									?>
																									</select>
																								</div>
																							</div>
																							<div class="uptArea hide">
																								<div class="form-group">
																									<label>Kanwil</label><br>
																									<div class="typeahead__container">
																										<div class="typeahead__field">
																											<div class="typeahead__query">
																												<input class="js-typeahead-car_v1" id="idUpt_Register" type="search" placeholder="Search" autocomplete="off">
																											</div>
																										</div>
																									</div>	                                            
																								</div>
																								<div class="form-group">
																									<label>Pilih UPT</label><br>
																									<select id="idUptKanwil" class="form-control"></select>
																								</div>
																								<div class="form-group">
																									<label>Pilih Jabatan</label><br>
																									<select id="idUptJabatan" class="form-control">
																									<option selected="true" disabled="disabled">-- pilih jabatan --</option>
																									<?php
																									foreach($upt_jabatan as $row){
																										?>
																										<option value="<?=$row['id_struktur_jabatan'];?>"><?=$row['nm_jabatan'];?></option>
																										<?php
																									}
																									?>
																									</select>
																								</div>
																							</div>
																						</div>
																					</div>			
																				</form>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="modal-footer">
													<button type="button" id="mutasiJabatan_btnSubmit_save" class="btn btn-primary waves-effect" data-dismiss="modal">Save</button>
													<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
												</div>
											</div>
											<!-- /.modal-content -->
										</div>
										<!-- /.modal-dialog -->
									</div>
									
									<div id="myModal2" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
													<h5 class="modal-title" id="myModalLabel">Edit Account</h5>
												</div>
												<div class="modal-body">
													<!-- Row -->
													<div class="row">
														<div class="col-lg-12">
															<div class="">
																<div class="panel-wrapper collapse in">
																	<div class="panel-body pa-0">
																		<div class="col-sm-12 col-xs-12">
																			<div class="form-wrap">
																				<form action="#">
																					<div class="form-body overflow-hide">
																						<div class="form-group">
																							<label class="control-label mb-10" for="profil_idUsername">Username</label>
																							<div class="input-group">
																								<div class="input-group-addon"><i class="icon-lock"></i></div>
																								<input type="text" disabled class="form-control" id="profil_idUsername" placeholder="Masukkan Username">
																							</div>
																						</div>
																						<div class="form-group">
																							<label class="control-label mb-10" for="profil_idPasswordLama">Password Lama</label>
																							<div class="input-group">
																								<div class="input-group-addon"><i class="icon-lock"></i></div>
																								<input type="password" class="form-control" id="profil_idPasswordLama" placeholder="Masukkan Password Lama">
																							</div>
																						</div>
																						<div class="form-group">
																							<label class="control-label mb-10" for="profil_idPasswordBaru">Password Baru</label>
																							<div class="input-group">
																								<div class="input-group-addon"><i class="icon-lock"></i></div>
																								<input type="password" class="form-control" id="profil_idPasswordBaru" placeholder="Masukkan Password Baru">
																							</div>
																						</div>
																					</div>			
																				</form>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="modal-footer">
													<button type="button" id="idProfile_btnSubmit_save_editAccount" class="btn btn-primary waves-effect" data-dismiss="modal">Save</button>
													<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
												</div>
											</div>
											<!-- /.modal-content -->
										</div>
										<!-- /.modal-dialog -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /Row -->
		
	</div>
	
</div>
<!-- /Main Content -->