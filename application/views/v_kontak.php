<!-- Main Content -->
<div class="page-wrapper">
	<div class="container-fluid">
		<!-- Row -->
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default card-view">
					<div class="panel-wrapper collapse in">
						<div class="panel-body">
							<div class="table-wrap">
							
								<?php
								if($level == 2 || $level == 1){
									?>
									<div  class="pills-struct mt-40">
										<ul role="tablist" class="nav nav-pills" id="myTabs_6">
											<li class="active" role="presentation"><a aria-expanded="true"  data-toggle="tab" role="tab" id="home_tab_6" href="#home_6">Approval</a></li>
											<li role="presentation" class=""><a  data-toggle="tab" id="profile_tab_6" role="tab" href="#profile_6" aria-expanded="false">All</a></li>
										</ul>
										<div class="tab-content" id="myTabContent_6">
											<div  id="home_6" class="tab-pane fade active in" role="tabpanel">
												<table id="footable_2" data-show-toggle="false" class="table" data-paging="true" data-filtering="true" data-sorting="true">
													<thead>
													<tr>
														<th data-type="html">&nbsp;</th>
														<th data-breakpoints="xs sm">&nbsp; </th>
													</tr>
													</thead>
													<tbody>
													
													<?php
													foreach($group_chat_user as $row){
														$temp_id	= $row['id_gcu']; //HARUSNYA ID GCU
														$temp_gc 	= $row['first_name'].''.$row['last_name'];
														// $temp_cat 	= 'Personal';//$row['category'];
														$temp_all	= $temp_id.'#'.$temp_gc;
														// $temp_all	= 100;
														?>
														<tr>
															<td><img style="vertical-align:middle;" src="<?=$row['pp']==''?base_url('assets/img/default-avatar.png'):base_url('uploads/images/pp/'.$row['pp'].'');?>" width="10%" class="img-circle"> &nbsp;&nbsp; <?=$row['first_name'].''.$row['last_name'];?> 
																<?php
																if($row['approval_status']==0){
																	?>
																	<button onclick="approval_user(<?=$row['id'];?>)" style="max-width:30px;max-height:30px;" class="btn btn-success btn-icon-anim btn-circle btn-xs pull-right"><i class="fa fa-check"></i></button>
																	<button onclick="delete_user(<?=$row['id'];?>)" style="max-width:30px;max-height:30px;" class="btn btn-danger btn-icon-anim btn-circle btn-xs pull-right"><i class="fa fa-times"></i></button>
																	<?php
																}else{
																	?>
																	<button onclick="openChat('<?=$temp_all;?>')" style="max-width:30px;max-height:30px;" class="btn btn-primary btn-icon-anim btn-circle btn-xs pull-right"><i class="fa fa-paper-plane"></i></button> 
																	<button style="max-width:30px;max-height:30px;" class="btn btn-primary btn-icon-anim btn-circle btn-xs pull-right"><i class="fa fa-info"></i></button>
																	<?php
																}
																?>
																
															</td>
														</tr>
													<?php
													}
													?>
													</tbody>
												</table>
											</div>
											<div  id="profile_6" class="tab-pane fade" role="tabpanel">
												<table id="footable_222" data-show-toggle="false" class="table" data-paging="true" data-filtering="true" data-sorting="true">
													<thead>
													<tr>
														<th data-type="html">&nbsp;</th>
														<th data-breakpoints="xs sm">&nbsp; </th>
													</tr>
													</thead>
													<tbody>
													
													<?php
													foreach($group_chat_user2 as $row){
														$temp_id	= $row['id_gcu']; //HARUSNYA ID GCU
														$temp_gc 	= $row['first_name'].''.$row['last_name'];
														// $temp_cat 	= 'Personal';//$row['category'];
														$temp_all	= $temp_id.'#'.$temp_gc;
														// $temp_all	= 100;
														?>
														<tr>
															<td><img style="vertical-align:middle;" src="<?=$row['pp']==''?base_url('assets/img/default-avatar.png'):base_url('uploads/images/pp/'.$row['pp'].'');?>" width="10%" class="img-circle"> &nbsp;&nbsp; <?=$row['first_name'].''.$row['last_name'];?> 
																<?php
																if($row['approval_status']==0){
																	?>
																	<button onclick="approval_user(<?=$row['id'];?>)" style="max-width:30px;max-height:30px;" class="btn btn-success btn-icon-anim btn-circle btn-xs pull-right"><i class="fa fa-check"></i></button>
																	<button style="max-width:30px;max-height:30px;" class="btn btn-primary btn-icon-anim btn-circle btn-xs pull-right"><i class="fa fa-info"></i></button>
																	<?php
																}else{
																	?>
																	<button onclick="openChat('<?=$temp_all;?>')" style="max-width:30px;max-height:30px;" class="btn btn-primary btn-icon-anim btn-circle btn-xs pull-right"><i class="fa fa-paper-plane"></i></button> 
																	<button style="max-width:30px;max-height:30px;" class="btn btn-primary btn-icon-anim btn-circle btn-xs pull-right"><i class="fa fa-info"></i></button>
																	<?php
																}
																?>
																
															</td>
														</tr>
													<?php
													}
													?>
													</tbody>
												</table>
											</div>
										</div>
									</div>
									<?php
								}else{
									//USER BIASA
									?>
									<table id="footable_2" data-show-toggle="false" class="table" data-paging="true" data-filtering="true" data-sorting="true">
										<thead>
										<tr>
											<th data-type="html">&nbsp;</th>
											<th data-breakpoints="xs sm">&nbsp; </th>
										</tr>
										</thead>
										<tbody>
										
										<?php
										foreach($group_chat_user as $row){
											$temp_id	= $row['id_gcu']; //HARUSNYA ID GCU
											$temp_gc 	= $row['first_name'].''.$row['last_name'];
											// $temp_cat 	= 'Personal';//$row['category'];
											$temp_all	= $temp_id.'#'.$temp_gc;
											// $temp_all	= 100;
											?>
											<tr>
												<td><img style="vertical-align:middle;" src="<?=base_url();?>assets/img/userimgs/user3.png" width="10%" class="img-circle"> &nbsp;&nbsp; <?=$row['first_name'].''.$row['last_name'];?> 
													<?php
													if($row['approval_status']==0){
														?>
														<button onclick="approval_user(<?=$row['id'];?>)" style="max-width:30px;max-height:30px;" class="btn btn-success btn-icon-anim btn-circle btn-xs pull-right"><i class="fa fa-check"></i></button>
														<button style="max-width:30px;max-height:30px;" class="btn btn-primary btn-icon-anim btn-circle btn-xs pull-right"><i class="fa fa-info"></i></button>
														<?php
													}else{
														?>
														<button onclick="openChat('<?=$temp_all;?>')" style="max-width:30px;max-height:30px;" class="btn btn-primary btn-icon-anim btn-circle btn-xs pull-right"><i class="fa fa-paper-plane"></i></button> 
														<button style="max-width:30px;max-height:30px;" class="btn btn-primary btn-icon-anim btn-circle btn-xs pull-right"><i class="fa fa-info"></i></button>
														<?php
													}
													?>
													
												</td>
											</tr>
										<?php
										}
										?>
										</tbody>
									</table>
									<?php
								}
								?>
									
								

								<!--Editor-->
								<div class="modal fade" id="editor-modal" tabindex="-1" role="dialog" aria-labelledby="editor-title">
								
								<div class="modal-dialog" role="document">
									<form class="modal-content form-horizontal" id="editor">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
											<h5 class="modal-title" id="editor-title">Add Row</h5>
										</div>
										<div class="modal-body">
											<input type="number" id="id" name="id" class="hidden"/>
											<div class="form-group required">
												<label for="firstName" class="col-sm-3 control-label">Nama Grup</label>
												<div class="col-sm-9">
													<input type="text" class="form-control" id="firstName" name="firstName" placeholder="Nama Grup" required>
												</div>
											</div>
											<div class="form-group required">
												<label for="lastName" class="col-sm-3 control-label">Deskripsi</label>
												<div class="col-sm-9">
													<input type="text" class="form-control" id="lastName" name="lastName" placeholder="Deskripsi" required>
												</div>
											</div>
											<div class="form-group">
												<label for="jobTitle" class="col-sm-3 control-label">Akses</label>
												<div class="col-sm-9">
													<input type="text" class="form-control" id="jobTitle" name="jobTitle" placeholder="Job Title">
													<input type="checkbox" name="akses" value="1">
												</div>
											</div>
										</div>
										<div class="modal-footer">
											<button type="submit" class="btn btn-primary">Save changes</button>
											<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
										</div>
									</form>
								</div>
							</div>
							<!--/Editor-->
						</div>
					</div>
				</div>
				</div>
			</div>
		</div>
		<!-- /Row -->
	</div>
</div