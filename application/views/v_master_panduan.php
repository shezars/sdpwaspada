<!-- Main Content -->
<div class="page-wrapper">
	<div class="container-fluid">
		<!-- Row -->
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default card-view">
					<div class="panel-wrapper collapse in">
						<div class="panel-body">
							<div class="table-wrap">
								
								Content Here
								
						</div>
					</div>
				</div>
				</div>
			</div>
		</div>
		<!-- /Row -->
		
		<!-- MODAL UBAH PENGGUNA -->
		<div id="ubahPengguna" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						<h5 class="modal-title">&nbsp;</h5>
					</div>
					<div class="modal-body">
						
						<form>
							<div class="form-group" align="center">
								
								<div class="image-upload">
								  <label for="file-input">
									<img src="<?=base_url();?>assets/img/default-avatar.png" width="50%"/>
								  </label>

								  <input id="file-input" type="file" style="display: none;" />
								</div>
							</div>
							<div class="form-group">
								<label class="control-label mb-10">Unit:</label>
								<select class="form-control">
									<option>--pilih unit--</option>
									<option>Ditjenpas</option>
									<option>Kanwil</option>
									<option>UPT</option>
								</select>
							</div>
							<div class="form-group">
								<label class="control-label mb-10">Nama Grup:</label>
								<input type="text" class="form-control" id="idNamaLengkap">
							</div>
							<button class="btn btn-primary btn-block" disabled>Simpan</button>
						</form>
						
					</div>
				</div>
			</div>
		</div>
		
		
		<!-- MODAL TAMBAH PENGGUNA -->
		<div id="tambahPengguna" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
			<div class="modal-dialog" style="width: 100%;height: 100%;margin: 0;padding: 10px;">
				<div class="modal-content" style="height: auto;min-height: 100%;border-radius: 0;">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						<h5 class="modal-title">&nbsp;</h5>
					</div>
					<div class="modal-body" style="height:500px;padding:0px;">
						<iframe src="<?=base_url();?>register/register_plain" frameborder="0" style="overflow:hidden;height:100%;width:100%" height="100%" width="100%">Your browser does not support iframes</iframe>
					</div>
				</div>
			</div>
		</div>
		
		
	</div>
</div