<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<title>SDP Waspada</title>
		<meta name="description" content="Grandin is a Dashboard & Admin Site Responsive Template by hencework." />
		<meta name="keywords" content="admin, admin dashboard, admin template, cms, crm, Grandin Admin, Grandinadmin, premium admin templates, responsive admin, sass, panel, software, ui, visualization, web app, application" />
		<meta name="author" content="hencework"/>
		
		<!-- Favicon -->
		<link rel="shortcut icon" href="favicon.ico">
		<link rel="icon" href="favicon.ico" type="image/x-icon">
		
		
		<!-- CSS Files -->
		<link href="<?=base_url();?>assets/register/css/bootstrap.min.css" rel="stylesheet" />
		<link href="<?=base_url();?>assets/register/css/paper-bootstrap-wizard.css" rel="stylesheet" />
		<link href="https://netdna.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.css" rel="stylesheet">
		<link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
		<link href="<?=base_url();?>assets/register/css/themify-icons.css" rel="stylesheet">
		
		<!-- JQuery Typeahead -->
		<link href="<?=base_url();?>assets/dist/jquerytypeahead/jquery.typeahead.min.css" rel="stylesheet" type="text/css"/>
	
	
		<style>
		.disabledTab {
			pointer-events: none;
		}
		</style>
	</head>
	<body style="background-color:#212121;">
		<!--Preloader-->
		<div class="preloader-it">
			<div class="la-anim-1"></div>
		</div>
		<!--/Preloader-->	
			<div align="center" style="padding-top:2rem;">
				<a href="<?=base_url();?>"><img class="brand-img mr-10" src="<?=base_url();?>assets/img/logo.png" alt="brand"/></a>
			</div>
			<!-- Main Content -->
			<div style="padding-top:2rem;" class="page-wrapper pa-0 ma-0 auth-page">
				<div class="container-fluid">


		                <div class="card wizard-card" data-color="orange" id="wizardProfile">
		                    <form action="" method="">
		                <!--        You can switch " data-color="orange" "  with one of the next bright colors: "blue", "green", "orange", "red", "azure"          -->

								<input type="hidden" id="idStatusResetPassword" value=0>
								<div class="wizard-navigation">
									<div class="progress-with-circle">
									     <div class="progress-bar" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="3" style="width: 21%;"></div>
									</div>
									<ul>
			                            <li class="disabledTab" id="idHeadInformasiAkun">
											<a href="#about" data-toggle="tab">
												<div class="icon-circle">
													<i class="ti-user"></i>
												</div>
												Informasi Akun
											</a>
										</li>
										<li class="disabledTab" id="idHeadKodeVerifikasi">
											<a href="#about2" data-toggle="tab">
												<div class="icon-circle">
													<i class="ti-write"></i>
												</div>
												Kode Verifikasi
											</a>
										</li>
			                            <li class="disabledTab" id="idHeadResetPassword">
											<a href="#address" data-toggle="tab">
												<div class="icon-circle">
													<i class="ti-lock"></i>
												</div>
												Reset Password
											</a>
										</li>
			                        </ul>
								</div>
		                        <div class="tab-content">
		                            <div class="tab-pane" id="about" style="padding-top:4rem;">
		                            	<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label>Masukkan NIP Anda <small>(required)</small></label>
													<input id="idAkun" type="number" class="form-control" placeholder="Isikan NIP Anda">
												</div>
											</div>
										</div>
		                            </div>
									<div class="tab-pane" id="about2" style="padding-top:4rem;">
		                            	<div class="row">
											<div class="col-sm-10 col-sm-offset-1">
												<div class="form-group">
													<label>Kode Verifikasi <small>(required)</small></label>
													<input id="idUser" required type="hidden">
													<input id="idKodeVerifikasi" required type="number" class="form-control" placeholder="Isikan Kode Verifikasi">
												</div>
											</div>
										</div>
		                            </div>
		                            <div class="tab-pane" id="address" style="padding-top:2rem;">
		                                <div class="row">
											<div class="col-sm-10 col-sm-offset-1">
												<div class="form-group">
													<label>Password Baru <small>(required)</small></label>
													<input id="idPasswordBaru" required type="password" class="form-control" placeholder="Masukkan Password Baru Anda">
												</div>
											</div>
										</div>
		                            </div>
		                        </div>
		                        <div class="wizard-footer">
		                            <div class="pull-right">
		                                <input type='button' class='btn btn-next btn-fill btn-warning btn-wd btnNext' name='next' value='Next' />
		                                <input type='button' class='btn btn-finish btn-fill btn-warning btn-wd finish' name='finish' value='Submit' />
		                            </div>

		                            <div class="pull-left">
		                                <input type='button' id="btnPrevious" class='btn btn-previous btn-default btn-wd' name='previous' value='Previous' style="visibility:hidden;"/>
		                            </div>
									
		                            <div class="clearfix"></div>
		                        </div>
		                    </form>
		                </div>
		            
				</div>
				
			</div>
			<!-- /Main Content -->
		
		
		<!--   Core JS Files   -->
		<script src="<?=base_url();?>assets/register/js/jquery-2.2.4.min.js" type="text/javascript"></script>
		<script src="<?=base_url();?>assets/register/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="<?=base_url();?>assets/register/js/jquery.bootstrap.wizard.js" type="text/javascript"></script>
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

		<!--  Plugin for the Wizard -->
		<script src="<?=base_url();?>assets/register/js/demo.js" type="text/javascript"></script>
		<script src="<?=base_url();?>assets/register/js/paper-bootstrap-wizard.js" type="text/javascript"></script>

		<!--  More information about jquery.validate here: https://jqueryvalidation.org/	 -->
		<script src="<?=base_url();?>assets/register/js/jquery.validate.min.js" type="text/javascript"></script>
		
		<!-- Select2 JavaScript -->
		<script src="<?=base_url();?>assets/dist/jquerytypeahead/jquery.typeahead.min.js"></script>
		
		<script>		
		$(".btnNext").click(function(){
			var akun = $("#idAkun").val();
			var status = $("#idStatusResetPassword").val();
			
			if(akun=='' || akun==null){
				swal("NIP Tidak Boleh Kosong", "Silakan Isi Kolom NIK", "error");
				return false;
			}else if(status==0){
				$.post('<?=base_url();?>register/get_user_by_nip',{nip:akun}).done(function(data){ 
					var obj = $.parseJSON(data);
					if(obj['status']==0){
						swal("NIP Tidak Ditemukan", "Silakan Masukkan NIK Kembali", "error");
						$("#idAkun").val('');
						$("#btnPrevious").trigger('click'); 
						return false;
					}
					
					swal("NIP Ditemukan", "Silakan Periksa E-Mail Untuk Mendapatkan Kode Verifikasi", "success");
					
					$("#idStatusResetPassword").val(1);
					$("#idUser").val(obj['data']['id']);
				})
			}
			
			if(status==1){
				var kodeVerifikasi = $("#idKodeVerifikasi").val();
				
				$.post('<?=base_url();?>register/verifikasi_reset_password',{kode:kodeVerifikasi,nip:akun}).done(function(data){ 
					var obj = $.parseJSON(data);
					
					if(obj['status']==0){
						swal("Kode Verifikasi Tidak Sesuai", "Silakan Masukkan Kode Verifikasi Kembali", "error");
						$("#idKodeVerifikasi").val('');
						$("#btnPrevious").trigger('click'); 
						return false;
					}
					
					$("#idStatusResetPassword").val(2);
				})
			}
			
		})
		
		$(".finish").click(function(){
			var idUser 			= $("#idUser").val();
			var akun 			= $("#idAkun").val();
			var passwordBaru 	= $("#idPasswordBaru").val();
				
			$.post('<?=base_url();?>register/proses_reset_password',{nip:akun,password:passwordBaru,idUser:idUser}).done(function(data){ 
				swal("Reset Password Berhasil", "Silakan Login Kembali", "success");
				setTimeout(window.location.href = '<?=base_url();?>' , 3000);
			})
		})
		
		</script>
	
	</body>
</html>
