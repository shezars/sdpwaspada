
    </div>
    <!-- /#wrapper -->
	
	<!-- JavaScript -->
	
    <!-- jQuery -->
    <script src="<?=base_url();?>assets/vendors/bower_components/jquery/dist/jquery.min.js"></script>

	<!-- Jquery Scroll Speed -->
	<script src="<?=base_url();?>assets/dist/js/jQuery.scrollSpeed.js"></script>
	
	<script>
	$(function() {  

		// Default
		jQuery.scrollSpeed(100, 1600);
		
		// Custom Easing
		// jQuery.scrollSpeed(100, 800, 'easeOutCubic');
		
	});
	</script>
	
    <!-- Bootstrap Core JavaScript -->
    <script src="<?=base_url();?>assets/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- JQuery Browser -->
	<script src="<?=base_url();?>assets/dist/js/jquery.browser.js"></script>
	<?php
	if($this->uri->segment(1)=='kontak' || $this->uri->segment(2)=='pengguna' || $this->uri->segment(2)=='grup'){
		?>
		<script src="<?=base_url();?>assets/vendors/bower_components/FooTable/compiled/footable.js" type="text/javascript"></script>
		<script src="<?=base_url();?>assets/dist/js/footable-data.js"></script>
		<script src="<?=base_url();?>assets/vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>
		<script src="<?=base_url();?>assets/vendors/bower_components/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>
		<script src="<?=base_url();?>assets/vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>
		<?php
	}else if($this->uri->segment(1)=='profil'){
		?>
		<script src="<?=base_url();?>assets/vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>
		<!-- Select2 JavaScript -->
		<script src="<?=base_url();?>assets/dist/jquerytypeahead/jquery.typeahead.min.js"></script>
		<?php
	}
	?>
	
	<!-- Slimscroll JavaScript -->
	<script src="<?=base_url();?>assets/dist/js/jquery.slimscroll.js"></script>
	
	<!-- Owl JavaScript -->
	<script src="<?=base_url();?>assets/vendors/bower_components/owl.carousel/dist/owl.carousel.min.js"></script>
	
	<!-- Fancy Dropdown JS -->
	<script src="<?=base_url();?>assets/dist/js/dropdown-bootstrap-extended.js"></script>
	
	<!-- Switchery JavaScript -->
	<script src="<?=base_url();?>assets/vendors/bower_components/switchery/dist/switchery.min.js"></script>
	
	<!-- Init JavaScript -->
	<script src="<?=base_url();?>assets/dist/js/init.js"></script>
	
</body>

</html>
