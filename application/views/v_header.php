<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<title>SDP Waspada</title>
	<meta name="description" content="Grandin is a Dashboard & Admin Site Responsive Template by hencework." />
	<meta name="keywords" content="admin, admin dashboard, admin template, cms, crm, Grandin Admin, Grandinadmin, premium admin templates, responsive admin, sass, panel, software, ui, visualization, web app, application" />
	<meta name="author" content="hencework"/>
	
	<!-- Favicon -->
	<link rel="shortcut icon" href="<?=base_url();?>assets/favicon2.ico">
	<link rel="icon" href="favicon.ico" type="image/x-icon">
	
	<!-- switchery CSS -->
	<link href="<?=base_url();?>assets/vendors/bower_components/switchery/dist/switchery.min.css" rel="stylesheet" type="text/css"/>
			
	<?php
	if($this->uri->segment(1)=='kontak' || $this->uri->segment(2)=='pengguna' || $this->uri->segment(1)=='profil'){
		?>
		<!-- Footable CSS -->
		<link href="<?=base_url();?>assets/vendors/bower_components/FooTable/compiled/footable.bootstrap.min.css" rel="stylesheet" type="text/css"/>
		<!--alerts CSS -->
		<link href="<?=base_url();?>assets/vendors/bower_components/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">
		
		
		<!-- Bootstrap Switches CSS -->
		<link href="<?=base_url();?>assets/vendors/bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
		
		<!-- JQuery Typeahead -->
		<link href="<?=base_url();?>assets/dist/jquerytypeahead/jquery.typeahead.min.css" rel="stylesheet" type="text/css"/>
		<?php
	}
	?>	
	
		
	<!-- Custom CSS -->
	<link href="<?=base_url();?>assets/dist/css/style.css" rel="stylesheet" type="text/css">
	
	<style>
		.img-holder{
			position: relative;text-align:center;
		}
		.idOverlayPreviewGambar{
			width:100%;position: absolute;height:100%;left: 0px;top: 0px;background-color: white;opacity: 0.7;
		}
		.img-holder > button{
			position: absolute;background:black;opacity:0.5;transform: translate(-50%,-50%);margin-right: -50%;top: 50%;left: 50%;
		}
	</style>
</head>