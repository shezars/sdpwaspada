<body scroll="no" style="overflow:hidden;">
	<!--Preloader-->
	<div class="preloader-it">
		<div class="la-anim-1"></div>
	</div>
	<!--/Preloader-->
    <div class="wrapper theme-1-active pimary-color-pink">
		
		<!-- Top Menu Items -->
		<nav class="navbar navbar-inverse navbar-fixed-top">
			<div class="mobile-only-brand pull-left">
				<div class="nav-header pull-left">
					<div class="logo-wrap">
						<a href="<?=base_url();?>">
							<img class="brand-img" style="width:20%;" src="<?=base_url();?>assets/img/logo.png" alt="brand"/>
							<!--<span class="brand-text">SDP</span>-->
						</a>
					</div>
				</div>	
				<a id="toggle_nav_btn" class="toggle-left-nav-btn inline-block ml-20 pull-left" href="javascript:void(0);"><i class="zmdi zmdi-menu"></i></a>
			</div>
		</nav>
		<!-- /Top Menu Items -->
		
		<!-- Left Sidebar Menu -->
		<div class="fixed-sidebar-left">
			<ul class="nav navbar-nav side-nav nicescroll-bar">
					<?php
					$ci =& get_instance();
					$id = $this->ion_auth->get_user_id();
					$data = $ci->db->query("SELECT * FROM users WHERE id=$id")->row();
					$pp = $data->pp; 
					?>
				
					<!-- User Profile -->
					<li>
						<div class="user-profile text-center">
							<a href="<?=base_url();?>profil"><img src="<?=$pp==''?base_url('assets/img/default-avatar.png'):base_url('uploads/images/pp/'.$pp.'');?>" alt="user_auth" class="user-auth-img img-circle"/></a>
							<div class="dropdown mt-5">
							<a href="#" class="dropdown-toggle pr-0 bg-transparent"><?=$this->ion_auth->user()->row()->first_name.$this->ion_auth->user()->row()->last_name;?></a><br/>
							<a href="#" class="dropdown-toggle pr-0 bg-transparent"><?=$this->ion_auth->get_users_groups()->row()->name;?></a>
							<ul class="dropdown-menu user-auth-dropdown" data-dropdown-in="flipInX" data-dropdown-out="flipOutX">
								<li>
									<a href="profile.html"><i class="zmdi zmdi-account"></i><span>Profile</span></a>
								</li>
								<li>
									<a href="#"><i class="zmdi zmdi-card"></i><span>my balance</span></a>
								</li>
								<li>
									<a href="inbox.html"><i class="zmdi zmdi-email"></i><span>Inbox</span></a>
								</li>
								<li>
									<a href="#"><i class="zmdi zmdi-settings"></i><span>Settings</span></a>
								</li>
								<li class="divider"></li>
								<li class="sub-menu show-on-hover">
									<a href="#" class="dropdown-toggle pr-0 level-2-drp"><i class="zmdi zmdi-check text-success"></i> available</a>
									<ul class="dropdown-menu open-left-side">
										<li>
											<a href="#"><i class="zmdi zmdi-check text-success"></i><span>available</span></a>
										</li>
										<li>
											<a href="#"><i class="zmdi zmdi-circle-o text-warning"></i><span>busy</span></a>
										</li>
										<li>
											<a href="#"><i class="zmdi zmdi-minus-circle-outline text-danger"></i><span>offline</span></a>
										</li>
									</ul>	
								</li>
								<li class="divider"></li>
								<li>
									<a href="#"><i class="zmdi zmdi-power"></i><span>Log Out</span></a>
								</li>
							</ul>
							</div>
						</div>
					</li>
					<!-- /User Profile -->
				<li class="navigation-header">
					<span>Main</span> 
					<i class="zmdi zmdi-more"></i>
				</li>
				<li>
					<a href="<?=base_url();?>profil"><div class="pull-left"><i class="zmdi zmdi-account mr-20"></i><span class="right-nav-text">Profil</span></div><div class="pull-right"></div><div class="clearfix"></div></a>
					<!--
					<a href="<?=base_url();?>siaranbaru"><div class="pull-left"><i class="zmdi zmdi-speaker mr-20"></i><span class="right-nav-text">Siaran Baru</span></div><div class="pull-right"></div><div class="clearfix"></div></a>
					-->
					<a href="<?=base_url();?>kontak"><div class="pull-left"><i class="zmdi zmdi-accounts-list-alt mr-20"></i><span class="right-nav-text">Kontak</span></div><div class="pull-right"></div><div class="clearfix"></div></a>
					<?php
					if($this->ion_auth->get_users_groups($this->ion_auth->get_user_id())->row()->name=='admin'){
						?>
						<!-- <a href="<?=base_url();?>grup"><div class="pull-left"><i class="zmdi zmdi-accounts mr-20"></i><span class="right-nav-text">Grup</span></div><div class="pull-right"></div><div class="clearfix"></div></a> -->
						<?php
					}
					?>
				</li>
				<li><hr class="light-grey-hr mb-10"/></li>
				<li class="navigation-header">
					<i class="zmdi zmdi-more"></i>
				</li>
				<!--
				<li>
					<a href="<?=base_url();?>pengaturan"><div class="pull-left"><i class="zmdi zmdi-settings mr-20"></i><span class="right-nav-text">Pengaturan</span></div><div class="clearfix"></div></a>
				</li>
				-->
				
				<?php
				$id = $this->ion_auth->get_user_id();
				$ci =& get_instance();
				$level = $ci->db->query('SELECT level FROM users WHERE id='.$id.'')->row()->level;
				
				//SUPER ADMIN
				if($level==2){
				?>
					<li class="navigation-header">
						<span>Admin Panel</span> 
						<i class="zmdi zmdi-more"></i>
					</li>
					<li>
						<a href="<?=base_url();?>master/pengguna"><div class="pull-left"><i class="zmdi zmdi-accounts-alt mr-20"></i><span class="right-nav-text">Master Pengguna</span></div><div class="clearfix"></div></a>
					</li>
					<li>
						<a href="<?=base_url();?>master/grup"><div class="pull-left"><i class="zmdi zmdi-account-box mr-20"></i><span class="right-nav-text">Master Grup</span></div><div class="clearfix"></div></a>
					</li>
					<li>
						<a href="<?=base_url();?>master/faq"><div class="pull-left"><i class="zmdi zmdi-pin-help mr-20"></i><span class="right-nav-text">Master FAQ</span></div><div class="clearfix"></div></a>
					</li>
					<!--<li>
						<a href="<?=base_url();?>master/panduan"><div class="pull-left"><i class="zmdi zmdi-info-outline mr-20"></i><span class="right-nav-text">Master Panduan</span></div><div class="clearfix"></div></a>
					</li>
					-->
					<li><hr class="light-grey-hr mb-10"/></li>
				<?php
				}
				?>
				<li>
					<a href="<?=base_url();?>faq"><div class="pull-left"><i class="zmdi zmdi-map mr-20"></i><span class="right-nav-text">FAQ</span></div><div class="clearfix"></div></a>
				</li>
				<li>
					<a href="<?=base_url();?>panduan"><div class="pull-left"><i class="zmdi zmdi-book mr-20"></i><span class="right-nav-text">Panduan</span></div><div class="clearfix"></div></a>
				</li>
				<li>
					<a href="<?=base_url();?>login/logout"><div class="pull-left"><i class="zmdi zmdi-power mr-20"></i><span class="right-nav-text">Keluar</span></div><div class="clearfix"></div></a>
				</li>
				
			</ul>
		</div>
		<!-- /Left Sidebar Menu -->