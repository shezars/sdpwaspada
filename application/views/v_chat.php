<!-- Main Content -->
		<div class="page-wrapper" style="position:relative;width:100%;">
            <div class="container-fluid">
				
				
				<!-- Row -->
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default border-panel card-view pa-0">
							<div class="panel-wrapper collapse in">
								<div class="panel-body pa-0 areaGroupChat">
									<div class="chat-cmplt-wrap chat-for-widgets-1">
										<div class="chat-box-wrap">
											<div>
												<form role="search" class="chat-search">
													<div class="input-group">
														<input id="example-input1-group21" name="example-input1-group2" class="form-control" placeholder="Search" type="text">
														<span class="input-group-btn">
														<button type="button" class="btn  btn-default"><i class="zmdi zmdi-search"></i></button>
														</span>
													</div>
												</form>
												<div class="chatapp-nicescroll-bar">
													<ul class="chat-list-wrap" id="idAreaGroupChat">
														<li class="chat-list">
															<div class="chat-body">
																<input type="hidden" id="idReceiver">
																<!--<input type="hidden" id="idGroupType">-->
																<?php
																foreach($group_chat_user as $row){
																	$temp_id	= $row['id'];
																	$temp_gc 	= $row['group_chat'];
																	$temp_cat 	= $row['category'];
																	$temp_all	= $temp_id.'#'.$temp_gc.'#'.$temp_cat;
																	?>
																	<a href="javascript:void(0)">
																		<div class="chat-data" onclick="chatNow('<?=$temp_all;?>')" id="<?=$row['id'].'#'.$row['group_chat'].'#'.$row['category'];?>">
																			<img class="user-img img-circle" src="<?=base_url();?>assets/img/default-avatar.png" alt="user"/>
																			<div class="user-data" style="margin-top:10px;">
																				<span class="capitalize-font"><?=$row['group_chat'];?></span>
																			</div>
																			<div class="status away"></div>
																			<div class="clearfix"></div>
																		</div>
																	</a>
																	<?php
																}
																?>
															</div>
														</li>
													</ul>
												</div>
											</div>
										</div>
										<div class="recent-chat-box-wrap">
											<div class="recent-chat-wrap">
												<div class="panel-heading ma-0 pt-15">
													<div class="goto-back">
														<a  id="goto_back_widget_1" href="javascript:void(0)" class="inline-block txt-grey">
															<i class="zmdi zmdi-chevron-left"></i>
														</a>	
														<span class="inline-block txt-dark" id="txtGroupName">Group Name</span>
														<a href="javascript:void(0)" id="idInformation" class="inline-block text-right txt-grey"><i class="zmdi zmdi-videocam"></i></a>
														<div class="clearfix"></div>
													</div>
												</div>
												<div class="panel-wrapper collapse in">
													<div class="panel-body pa-0">
														<div class="chat-content">
															<ul class="chatapp-chat-nicescroll-bar pt-20 areaGroupChatContent" style="height:200px !important">
																<div id="idFriendArea"></div>
															</ul>
														</div>
														<div class="input-group">
															<input type="text" id="input_msg_send_chatapp" name="send-msg" class="input-msg-send form-control" placeholder="Type a message">
															<div class="input-group-btn emojis">
																<div class="fileupload btn btn-default"><i class="zmdi zmdi-attachment-alt"></i>
																	<input type="file" id="msg_attachment" name="file" class="upload">
																</div>
															</div>
															<div class="input-group-btn attachment">
																<div class="dropup">
																	<button type="button" id="button_msg_send_chatapp" class="btn btn-default "><i class="zmdi zmdi-mail-send"></i></button>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /Row -->
			</div>
        </div>
        <!-- /Main Content -->