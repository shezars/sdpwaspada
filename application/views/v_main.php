<?php
$this->load->view('v_header');
$this->load->view('v_sidebar');
$this->load->view($page);
$this->load->view('v_footer');
?>
<script>

function indonesia(){
	return 'INDONESIA';
}

function chatNow(all){
	// var id 			= this.id;
	var id 			= all;
	var idPecah		= id.split("#");
	console.log(idPecah);
	
	var group 		= idPecah[1];
	// var groupType 	= idPecah[2];
	
	// if(groupType=='personal'){
		// var id 			= '<?=$_SESSION['user_id'];?>';
	// }else{
		// var id 			= idPecah[0];
	// }
	
	//ID DISINI ADALAH ID GCU
	id 				= idPecah[0];
	
	$('#txtGroupName').text(group);
	$('#idReceiver').val(id);
	// $('#idGroupType').val(groupType);
	
	//GET DATA CHAT
	// $.post('<?=base_url();?>main/get_chat',{id:id,groupType:groupType}).done(function(data){
	$.post('<?=base_url();?>main/get_chat',{id:id}).done(function(data){
		var obj = jQuery.parseJSON(data);
		var html='';
		var receiver_id = obj['receiver_id'];
		
		for(x=0;x<obj['jumlah'];x++){
			var isipesan 	= '';
			var jenispesan 	= '';
			var imageIcon 	= '';
			var align 		= 'left';
			var nama 		= '';
			var waktu		= '';
			var size		= '';
			var path_real	= '';
			var path_real_video	= '';
			var id_chat_status	= '';
			
			if(obj['jumlah']>1){
				waktu 			= obj['data_chat'][x]['created_at'];
				id_chat_status 	= obj['data_chat'][x]['id_chat_status'];
				path_real 		= '<?=base_url();?>uploads/images/real/'+obj['data_chat'][x]['msg'];
				path_real_video	= '<?=base_url();?>uploads/videos/'+obj['data_chat'][x]['msg'];
				path_thumb_video = '<?=base_url();?>uploads/videos/thumbnail/'+obj['data_chat'][x]['thumbnail_pic'];
				filename 		= obj['data_chat'][x]['msg'];
				if(obj['data_chat'][x]['size']>1000){
					size 		= Math.round(obj['data_chat'][x]['size']/1000) +' MB';
				}else{
					size 		= obj['data_chat'][x]['size'] +' KB';
				}
				
				
				if(obj['data_chat'][x]['msg_type']=='text'){
					isipesan = obj['data_chat'][x]['msg'];
				}else if(obj['data_chat'][x]['msg_type']=='image'){
					if(obj['data_chat'][x]['flag_download']==1){
						isipesan = '<img onclick="previewImage(\''+filename+'\')" width="100%" src="<?=base_url('uploads/images/real/');?>'+obj['data_chat'][x]['msg']+'">';
					}else{
						isipesan = '<div id="id-imgHolder-'+obj['data_chat'][x]['id_chat_status']+'" onclick="downloadImage(\''+path_real+'\','+id_chat_status+')" class="img-holder"><img onclick="previewImage(\''+filename+'\')" id="idImagePreview-'+obj['data_chat'][x]['id_chat_status']+'" width="100%" src="<?=base_url('uploads/images/compress/');?>'+obj['data_chat'][x]['msg']+'"><div class="idOverlayPreviewGambar" id="idOverlayPreviewGambar-'+obj['data_chat'][x]['id_chat_status']+'"></div><button id="btnDownloadImage-'+obj['data_chat'][x]['id_chat_status']+'" class="btn btn-xs btn-rounded"><i class="fa fa-arrow-down"></i>&nbsp; '+size+'</button></div>';
					}
					
					// isipesan = 'INDONESIA';//'<img width="100%" src="<?=base_url('uploads/images/');?>'+obj['data_chat'][x]['msg']+'">';
					// isipesan = "<img src='file:///storage/emulated/0/Pictures/kodok.jpg'>";//'<img width="100%" src="<?=base_url('uploads/images/');?>'+obj['data_chat'][x]['msg']+'">';
				}else if(obj['data_chat'][x]['msg_type']=='video'){
					if(obj['data_chat'][x]['flag_download']==1){
						isipesan = '<img onclick="previewVideo(\''+filename+'\')" width="100%" src="<?=base_url('uploads/videos/thumbnail/');?>'+obj['data_chat'][x]['thumbnail_pic']+'">';
					}else{
						isipesan = '<div id="id-imgHolder-'+obj['data_chat'][x]['id_chat_status']+'" onclick="downloadImage(\''+path_real_video+'\','+id_chat_status+',\''+path_thumb_video+'\')" class="img-holder"><img onclick="previewVideo(\''+filename+'\')" id="idImagePreview-'+obj['data_chat'][x]['id_chat_status']+'" width="100%" src="<?=base_url('uploads/videos/thumbnail/');?>'+obj['data_chat'][x]['thumbnail_pic']+'"><div class="idOverlayPreviewGambar" id="idOverlayPreviewGambar-'+obj['data_chat'][x]['id_chat_status']+'"></div><button id="btnDownloadImage-'+obj['data_chat'][x]['id_chat_status']+'" class="btn btn-xs btn-rounded"><i class="fa fa-arrow-down"></i>&nbsp; '+size+'</button></div>';
					}
				}else{
					isipesan = '<a target="_BLANK" href="<?=base_url('uploads/documents/');?>'+obj['data_chat'][x]['msg']+'">'+obj['data_chat'][x]['msg']+'</a>';
				}
				
				if(obj['data_chat'][x]['sender_id'] == receiver_id){
					jenispesan = 'self';
					align = 'right';
				}else{
					jenispesan = 'friend';
					imageIcon = '<img class="user-img img-circle block pull-left"  src="<?=base_url();?>assets/img/default-avatar.png" alt="user"/>';
					nama = obj['data_chat'][x]['sender'];
				}
				
				//Update Flag
				if(obj['data_chat'][x]['flag_user']==0){
					$.post('<?=base_url();?>main/update_flag',{id:obj['data_chat'][x]['id']});
				}
			}else{
				waktu = obj['data_chat']['created_at'];
				id_chat_status 	= obj['data_chat']['id_chat_status'];
				path_real 		= '<?=base_url();?>uploads/images/real/'+obj['data_chat']['msg'];
				path_real_video	= '<?=base_url();?>uploads/videos/'+obj['data_chat']['msg'];
				path_thumb_video = '<?=base_url();?>uploads/videos/thumbnail/'+obj['data_chat']['thumbnail_pic'];
				filename 		= obj['data_chat']['msg'];
				
				if(obj['data_chat']['size']>1000){
					size 	= obj['data_chat']['size']/1000 +' MB';
				}else{
					size 	= obj['data_chat']['size'] +' KB';
				}
				
				if(obj['data_chat']['msg_type']=='text'){
					isipesan = obj['data_chat']['msg'];
				}else if(obj['data_chat']['msg_type']=='image'){
					if(obj['data_chat']['flag_download']==1){
						isipesan = '<img onclick="previewImage(\''+filename+'\')" width="100%" src="<?=base_url('uploads/images/real/');?>'+obj['data_chat']['msg']+'">';
					}else{
						isipesan = '<div id="id-imgHolder-'+obj['data_chat']['id_chat_status']+'" onclick="downloadImage(\''+path_real+'\','+id_chat_status+')" class="img-holder"><img onclick="previewImage(\''+filename+'\')" id="idImagePreview-'+obj['data_chat']['id_chat_status']+'" width="100%" src="<?=base_url('uploads/images/compress/');?>'+obj['data_chat']['msg']+'"><div class="idOverlayPreviewGambar" id="idOverlayPreviewGambar-'+obj['data_chat']['id_chat_status']+'"></div><button id="btnDownloadImage-'+obj['data_chat']['id_chat_status']+'" class="btn btn-xs btn-rounded"><i class="fa fa-arrow-down"></i>&nbsp; '+size+'</button></div>';
					}
				}else if(obj['data_chat']['msg_type']=='video'){
					if(obj['data_chat']['flag_download']==1){
						isipesan = '<img onclick="previewVideo(\''+filename+'\')" width="100%" src="<?=base_url('uploads/videos/thumbnail/');?>'+obj['data_chat']['thumbnail_pic']+'">';
					}else{
						isipesan = '<div id="id-imgHolder-'+obj['data_chat']['id_chat_status']+'" onclick="downloadImage(\''+path_real_video+'\','+id_chat_status+',\''+path_thumb_video+'\')" class="img-holder"><img onclick="previewVideo(\''+filename+'\')" id="idImagePreview-'+obj['data_chat'][x]['id_chat_status']+'" width="100%" src="<?=base_url('uploads/videos/thumbnail/');?>'+obj['data_chat']['thumbnail_pic']+'"><div class="idOverlayPreviewGambar" id="idOverlayPreviewGambar-'+obj['data_chat']['id_chat_status']+'"></div><button id="btnDownloadImage-'+obj['data_chat']['id_chat_status']+'" class="btn btn-xs btn-rounded"><i class="fa fa-arrow-down"></i>&nbsp; '+size+'</button></div>';
					}
				}else{
					isipesan = '<a target="_BLANK" href="<?=base_url('uploads/documents/');?>'+obj['data_chat']['msg']+'">'+obj['data_chat']['msg']+'</a>';
				}
				
				
				if(obj['data_chat']['sender_id'] == receiver_id){
					jenispesan = 'self';
					align = 'right';
				}else{
					jenispesan = 'friend';
					imageIcon = '<img class="user-img img-circle block pull-left"  src="<?=base_url();?>assets/img/default-avatar.png" alt="user"/>';
					nama = obj['data_chat']['sender'];
				}
				
				//Update Flag
				if(obj['data_chat']['flag_user']==0){
					$.post('<?=base_url();?>main/update_flag',{id:obj['data_chat']['id']});
				}
			}
			
			
			
			html += '<li class="'+jenispesan+'">'+
						'<div class="'+jenispesan+'-msg-wrap">'+
							imageIcon+
							'<div class="msg pull-'+align+'"><span style="font-size:7pt;">'+nama+'</span>'+
								'<p>'+isipesan+'</p>'+
								'<div class="msg-per-detail text-right">'+
									'<span class="msg-time txt-grey">'+waktu+'</span>'+
								'</div>'+
							'</div>'+
						'<div class="clearfix"></div>'+
						'</div>'+
					'</li>';
		}
		$("#idFriendArea").html(html);
	}).done(function(){
		/* Loop Get Chat */
		setInterval(function(){ get_chat(id) }, 2000);
		// setTimeout(function(){ get_chat(id) }, 2000);
	})
	
	scrollDown();
}

function get_chat(id){
	$.post('<?=base_url();?>main/get_chat_specific',{id:id}).done(function(data){
		var obj = jQuery.parseJSON(data);
		var html='';
		var receiver_id = obj['receiver_id'];
		
		for(x=0;x<obj['jumlah'];x++){
			var isipesan 	= '';
			var jenispesan 	= '';
			var imageIcon 	= '';
			var align 		= 'left';
			var nama 		= '';
			var waktu		= '';
			var size		= '';
			var path_real	= '';
			var path_real_video	= '';
			var id_chat_status	= '';
			
			if(obj['jumlah']>1){
				waktu = obj['data_chat'][x]['created_at'];
				id_chat_status 	= obj['data_chat'][x]['id_chat_status'];
				path_real 		= '<?=base_url();?>uploads/images/real/'+obj['data_chat'][x]['msg'];
				path_real_video	= '<?=base_url();?>uploads/videos/'+obj['data_chat'][x]['msg'];
				path_thumb_video = '<?=base_url();?>uploads/videos/thumbnail/'+obj['data_chat'][x]['thumbnail_pic'];
				filename 		= obj['data_chat'][x]['msg'];
				
				if(obj['data_chat'][x]['size']>1000){
					size 	= obj['data_chat'][x]['size']/1000 +' MB';
				}else{
					size 	= obj['data_chat'][x]['size'] +' KB';
				}
				
				if(obj['data_chat'][x]['msg_type']=='text'){
					isipesan = obj['data_chat'][x]['msg'];
				}else if(obj['data_chat'][x]['msg_type']=='image'){
					if(obj['data_chat'][x]['flag_download']==1){
						isipesan = '<img onclick="previewImage(\''+filename+'\')" width="100%" src="<?=base_url('uploads/images/real/');?>'+obj['data_chat'][x]['msg']+'">';
					}else{
						isipesan = '<div id="id-imgHolder-'+obj['data_chat'][x]['id_chat_status']+'" onclick="downloadImage(\''+path_real+'\','+id_chat_status+')" class="img-holder"><img onclick="previewImage(\''+filename+'\')" id="idImagePreview-'+obj['data_chat'][x]['id_chat_status']+'" width="100%" src="<?=base_url('uploads/images/compress/');?>'+obj['data_chat'][x]['msg']+'"><div class="idOverlayPreviewGambar" id="idOverlayPreviewGambar-'+obj['data_chat'][x]['id_chat_status']+'"></div><button id="btnDownloadImage-'+obj['data_chat'][x]['id_chat_status']+'" class="btn btn-xs btn-rounded"><i class="fa fa-arrow-down"></i>&nbsp; '+size+'</button></div>';
					}
				}else if(obj['data_chat'][x]['msg_type']=='video'){
					if(obj['data_chat'][x]['flag_download']==1){
						isipesan = '<img onclick="previewVideo(\''+filename+'\')" width="100%" src="<?=base_url('uploads/videos/thumbnail/');?>'+obj['data_chat'][x]['thumbnail_pic']+'">';
					}else{
						isipesan = '<div id="id-imgHolder-'+obj['data_chat'][x]['id_chat_status']+'" onclick="downloadImage(\''+path_real_video+'\','+id_chat_status+',\''+path_thumb_video+'\')" class="img-holder"><img onclick="previewVideo(\''+filename+'\')" id="idImagePreview-'+obj['data_chat'][x]['id_chat_status']+'" width="100%" src="<?=base_url('uploads/videos/thumbnail/');?>'+obj['data_chat'][x]['thumbnail_pic']+'"><div class="idOverlayPreviewGambar" id="idOverlayPreviewGambar-'+obj['data_chat'][x]['id_chat_status']+'"></div><button id="btnDownloadImage-'+obj['data_chat'][x]['id_chat_status']+'" class="btn btn-xs btn-rounded"><i class="fa fa-arrow-down"></i>&nbsp; '+size+'</button></div>';
					}
				}else{
					isipesan = '<a target="_BLANK" href="<?=base_url('uploads/documents/');?>'+obj['data_chat'][x]['msg']+'">'+obj['data_chat'][x]['msg']+'</a>';
				}
				
				if(obj['data_chat'][x]['sender_id'] == receiver_id){
					jenispesan = 'self';
					align = 'right';
				}else{
					jenispesan = 'friend';
					imageIcon = '<img class="user-img img-circle block pull-left"  src="<?=base_url();?>assets/img/default-avatar.png" alt="user"/>';
					nama = obj['data_chat'][x]['sender'];
				}
				
				//Update Flag
				if(obj['data_chat'][x]['flag_user']==0){
					$.post('<?=base_url();?>main/update_flag',{id:obj['data_chat'][x]['id']});
				}
			}else{
				waktu = obj['data_chat']['created_at'];
				id_chat_status 	= obj['data_chat']['id_chat_status'];
				path_real 		= '<?=base_url();?>uploads/images/real/'+obj['data_chat']['msg'];
				path_real_video	= '<?=base_url();?>uploads/videos/'+obj['data_chat']['msg'];
				path_thumb_video = '<?=base_url();?>uploads/videos/thumbnail/'+obj['data_chat']['thumbnail_pic'];
				filename 		= obj['data_chat']['msg'];
				
				if(obj['data_chat']['size']>1000){
					size 	= obj['data_chat']['size']/1000 +' MB';
				}else{
					size 	= obj['data_chat']['size'] +' KB';
				}
				
				if(obj['data_chat']['msg_type']=='text'){
					isipesan = obj['data_chat']['msg'];
				}else if(obj['data_chat']['msg_type']=='image'){
					if(obj['data_chat']['flag_download']==1){
						isipesan = '<img onclick="previewImage(\''+filename+'\')" width="100%" src="<?=base_url('uploads/images/real/');?>'+obj['data_chat']['msg']+'">';
					}else{
						isipesan = '<div id="id-imgHolder-'+obj['data_chat']['id_chat_status']+'" onclick="downloadImage(\''+path_real+'\','+id_chat_status+')" class="img-holder"><img onclick="previewImage(\''+filename+'\')" id="idImagePreview-'+obj['data_chat']['id_chat_status']+'" width="100%" src="<?=base_url('uploads/images/compress/');?>'+obj['data_chat']['msg']+'"><div class="idOverlayPreviewGambar" id="idOverlayPreviewGambar-'+obj['data_chat']['id_chat_status']+'"></div><button id="btnDownloadImage-'+obj['data_chat']['id_chat_status']+'" class="btn btn-xs btn-rounded"><i class="fa fa-arrow-down"></i>&nbsp; '+size+'</button></div>';
					}
				}else if(obj['data_chat']['msg_type']=='video'){
					if(obj['data_chat']['flag_download']==1){
						isipesan = '<img onclick="previewVideo(\''+filename+'\')" width="100%" src="<?=base_url('uploads/videos/thumbnail/');?>'+obj['data_chat']['thumbnail_pic']+'">';
					}else{
						isipesan = '<div id="id-imgHolder-'+obj['data_chat']['id_chat_status']+'" onclick="downloadImage(\''+path_real_video+'\','+id_chat_status+',\''+path_thumb_video+'\')" class="img-holder"><img onclick="previewImage(\''+filename+'\')" id="idImagePreview-'+obj['data_chat']['id_chat_status']+'" width="100%" src="<?=base_url('uploads/videos/thumbnail/');?>'+obj['data_chat']['thumbnail_pic']+'"><div class="idOverlayPreviewGambar" id="idOverlayPreviewGambar-'+obj['data_chat']['id_chat_status']+'"></div><button id="btnDownloadImage-'+obj['data_chat']['id_chat_status']+'" class="btn btn-xs btn-rounded"><i class="fa fa-arrow-down"></i>&nbsp; '+size+'</button></div>';
					}
				}else{
					isipesan = '<a target="_BLANK" href="<?=base_url('uploads/documents/');?>'+obj['data_chat']['msg']+'">'+obj['data_chat']['msg']+'</a>';
				}
				
				if(obj['data_chat']['sender_id'] == receiver_id){
					jenispesan = 'self';
					align = 'right';
				}else{
					jenispesan = 'friend';
					imageIcon = '<img class="user-img img-circle block pull-left"  src="<?=base_url();?>assets/img/default-avatar.png" alt="user"/>';
					nama = obj['data_chat']['sender'];
				}
				
				//Update Flag
				if(obj['data_chat']['flag_user']==0){
					$.post('<?=base_url();?>main/update_flag',{id:obj['data_chat']['id']});
				}
			}
			
			html += '<li class="'+jenispesan+'">'+
						'<div class="'+jenispesan+'-msg-wrap">'+
							imageIcon+
							'<div class="msg pull-'+align+'"><span style="font-size:7pt;">'+nama+'</span>'+
								'<p>'+isipesan+'</p>'+
								'<div class="msg-per-detail text-right">'+
									'<span class="msg-time txt-grey">2:35 pm</span>'+
								'</div>'+
							'</div>'+
						'<div class="clearfix"></div>'+
						'</div>'+
					'</li>';
			
			scrollDown();
		}
		$("#idFriendArea").append(html);
	})
}

function openChat(all){
	document.cookie = "core="+all;	
	document.cookie = "chatActive=true";
	window.location.href = '<?=base_url();?>';
}

$(document).ready( function() {
	if(getCookie('chatActive')=='true'){
		var data = getCookie('core');
		chatNow(getCookie('core'));
		chatAppTarget.addClass('chat-box-slide');
		document.cookie = "chatActive=false";	
		document.cookie = "core=";	
	}
})

function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function delete_cookie( name, path, domain ) {
  if( get_cookie( name ) ) {
    document.cookie = name + "=" +
      ((path) ? ";path="+path:"")+
      ((domain)?";domain="+domain:"") +
      ";expires=Thu, 01 Jan 1970 00:00:01 GMT";
  }
}

function approval_user(id){
	swal({   
            title: "Apakah anda yakin?",   
            text: "Apakah anda yakin untuk menyetujui pengguna ini?",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#e3c94b",   
            confirmButtonText: "Iya",   
            cancelButtonText: "Tidak",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) { 
				$.post('<?=base_url();?>master/approve_pengguna',{id:id}).done(function(data){
					swal("Approval Berhasil!", "Pengguna berhasil disetujui", "success");  
					location.reload();					
				})
            }else{
				swal.close();
			}
			// else {     
                // swal("Cancelled", "Your imaginary file is safe :)", "error");   
            // } 
        });
}

function delete_user(id){
		swal({   
            title: "Apakah anda yakin?",   
            text: "Apakah anda yakin untuk menolak persetujuan pengguna ini?",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#e3c94b",   
            confirmButtonText: "Iya",   
            cancelButtonText: "Tidak",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) { 
				$.post('<?=base_url();?>master/approve_pengguna/tolak',{id:id}).done(function(data){
					swal("Approval Berhasil!", "Pengguna berhasil ditolak", "success");  
					location.reload();					
				})
            }else{
				swal.close();
			}
        });
}

$('.upload').change(function(){
	jQuery.ajax({
        type: 'POST',
        url:"<?=base_url();?>profil/update_photo",
        data: new FormData($("#infoForm1")[0]),
        processData: false, 
        contentType: false, 
        success: function(filename) {
			if(filename==0){
				
			}else{
				d = new Date();
				$("#idPP_Profile").attr("src","<?=base_url();?>uploads/images/pp/"+filename+"?"+d.getTime());
			}
			
			 // console.log(returnval);
             // $("#show1").html(returnval);
             // $('#show1').show();
         }
    });
})

$('.editProfile').click(function(){
	var complete_name 	= $("#idProfile_complete_name").text();
	var description		= $("#idProfile_description").text();
	var jabatan 		= $("#idProfile_jabatan").text();
	var telp 			= $("#idProfile_telp").text();
	
	$("#profil_idNamaLengkap").val(complete_name);
	$("#profil_idJabatan").val(jabatan);
	$("#profil_idTelepon").val(telp);
	
})
$("#idProfile_btnSubmit_save").click(function(){
	var namaLengkap = $("#profil_idNamaLengkap").val();
	var telepon 	= $("#profil_idTelepon").val();
	$.post('<?=base_url();?>profil/update_profile',{namalengkap:namaLengkap,telepon:telepon}).done(function(data){
		$("#idProfile_complete_name").text(namaLengkap);
		$("#idProfile_telp").text(telepon);
	})
})
$('.editAccount').click(function(){
	$.post('<?=base_url();?>profil/get_user').done(function(data){
		var obj = $.parseJSON(data);
		$("#profil_idUsername").val(obj.username);
	})
})
$("#idProfile_btnSubmit_save_editAccount").click(function(){
	var oldPassword = $("#profil_idPasswordLama").val();
	var newPassword = $("#profil_idPasswordBaru").val();
	$.post('<?=base_url();?>profil/update_password',{oldPassword:oldPassword,newPassword:newPassword}).done(function(data){
		if(data==99){
			swal("Informasi!", "Password Lama Tidak Sesuai", "error");  
		}else{
			swal({
				title: "Informasi!",
				text: "Ubah Password Berhasil",
				type: "success",
				confirmButtonText: "OK"
			},function(isConfirm){
			  if (isConfirm) {
				window.location.href = "<?=base_url();?>login/logout";
			  }
			})
		}
	})
})
$("#idBtnModal_ubahjabatan").click(function(){
	$("#idModalBody_ubahprofile").addClass('hide');
	$("#idModalBody_ubahjabatan").removeClass('hide');
})
$(".pilihUnit").change(function(){
	var value = this.value;
	if(value==1){
		$(".ditjenpasArea").removeClass('hide');
		$(".kanwilArea").addClass('hide');
		$(".uptArea").addClass('hide');
	}else if(value==2){
		$(".ditjenpasArea").addClass('hide');
		$(".kanwilArea").removeClass('hide');
		$(".uptArea").addClass('hide');
	}else if(value==3){
		$(".ditjenpasArea").addClass('hide');
		$(".kanwilArea").addClass('hide');
		$(".uptArea").removeClass('hide');
	}else{
		$(".ditjenpasArea").addClass('hide');
		$(".kanwilArea").addClass('hide');
		$(".uptArea").addClass('hide');
	}
})
$("#idDirektorat").change(function(){
	var id = this.id;
	id = $("#"+id).val();
	$.post('<?=base_url();?>register/get_subdirektorat',{id:id}).done(function(data){
		var obj = jQuery.parseJSON(data);
		$("#idSubDirektorat").html($("<option></option>").attr("value","").text(""));
		$("#idSubDirektorat").html($("<option selected='true' disabled='disabled'></option>").attr("value","").text("-- pilih subdirektorat --"));
		$.each(obj,function(key,value){
			$("#idSubDirektorat").append($("<option></option>").attr("value",value.id_struktur).text(value.nm_struktur));
		})
		$("#idAreaSubDirektorat").removeClass('hide');
	})
})
$("#idSubDirektorat").change(function(){
	var id = this.id;
	id = $("#"+id).val();
	$.post('<?=base_url();?>register/get_jabatan',{id:id}).done(function(data){
		var obj = jQuery.parseJSON(data);
		
		$("#idJabatan").html($("<option></option>").attr("value","").text(""));
		$("#idJabatan").html($("<option selected='true' disabled='disabled'></option>").attr("value","").text("-- pilih jabatan --"));
		$.each(obj,function(key,value){
			$("#idJabatan").append($("<option></option>").attr("value",value.id_struktur_jabatan).text(value.nm_jabatan));
		})
		$("#idAreaJabatan").removeClass('hide');
	})
})
$.typeahead({
	input: '.js-typeahead-car_v1',
	minLength: 1,
	order: "asc",
	offset: false,
	hint: true,
	source: {
		car: {
			data: ["My first added brand", "M1 added brand at start"],
			ajax: {
				type: "POST",
				url: "<?=base_url();?>register/get_kanwil_uraian",
				data: {
					myKey: "myValue"
				}
			}
		}
	},
	callback: {
		onClick: function (node, a, item, event) {
			if(node[0].id == 'idKanwil_Register'){
				$("#idAreaJabatan_register2").removeClass("hide");
			}else{
				var kanwil = item.display;
				$.post('<?=base_url();?>register/get_upt_by_kanwil',{kanwil:kanwil}).done(function(data){
					var obj = jQuery.parseJSON(data);
					$("#idUptKanwil").html($("<option></option>").attr("value","0").text("-- Pilih UPT --"));
					$.each(obj,function(key,value){
						$("#idUptKanwil").append($("<option></option>").attr("value",value.id_upt).text(value.uraian));
					});
					$("#idAreaUpt").removeClass("hide");
				})
			}
		}
	}
});
$("#mutasiJabatan_btnSubmit_save").click(function(){
	var value = $(".pilihUnit").val();
	if(value==1){
		//DITJENPAS
		var direktorat 		= $("#idDirektorat").val();
		var subdirektorat 	= $("#idSubDirektorat").val();
		var jabatan 		= $("#idJabatan").val();
		
	}else if(value==2){
		//KANWIL
		var kanwil 			= $("#idKanwil_Register").val();
		var kanwilJabatan 	= $("#idKanwilJabatan").val();
		
	}else if(value==3){
		//UPT
		var kanwil 			= $("#idUpt_Register").val();
		var upt 			= $("#idUptKanwil").val();
		var uptJabatan	 	= $("#idUptJabatan").val();
		
	}else{
		alert("Harap Memilih Unit!");
		return false;
	}
})
</script>
