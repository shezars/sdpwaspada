<!-- Main Content -->
<div class="page-wrapper">
	<div class="container-fluid">
		<!-- Row -->
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default card-view">
					<div class="panel-wrapper collapse in">
						<div class="panel-body">
							<div class="table-wrap">
								<table id="footable_2" data-show-toggle="false" class="table" data-paging="true" data-filtering="true" data-sorting="false">
									<thead>
									<tr>
										<th data-type="html">&nbsp;</th>
										<th data-type="html">&nbsp;</th>
										<th data-breakpoints="xs sm">&nbsp; </th>
									</tr>
									</thead>
									<tbody>
									<?php
									foreach($get_all_faq as $row){
										// $temp_id		= $row['id'];
										// $temp_gc 	= $row['group_chat'];
										// $temp_cat 	= $row['category'];
										// $temp_all	= $temp_id.'#'.$temp_gc.'#'.$temp_cat;
										?>
										<tr>
											<td>
												<?=$row['judul'];?> 
											</td>
											<td>
											<?php
											
												echo '<button style="max-width:30px;max-height:30px;" id="'.$row['id'].'" data-toggle="modal" data-target="#ubahFAQ" class="btn btn-primary btn-icon-anim btn-circle btn-xs ubahFAQ"><i class="fa fa-edit"></i></button>';
												echo '<button style="max-width:30px;max-height:30px;" id="'.$row['id'].'" class="btn btn-danger btn-icon-anim btn-circle btn-xs hapusFAQ"><i class="fa fa-trash"></i></button>';
												
												?>
											</td>
										</tr>
									<?php
									}
									?>
									</tbody>
								</table>
								<button class="btn btn-block btn-primary" data-toggle="modal" data-target="#tambahPengguna">TAMBAH FAQ</button>
						</div>
					</div>
				</div>
				</div>
			</div>
		</div>
		<!-- /Row -->
		
		<!-- MODAL UBAH FAQ -->
		<div id="ubahFAQ" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						<h5 class="modal-title">&nbsp;</h5>
					</div>
					<div class="modal-body">
						<form method="POST" action="<?=base_url();?>master/upd_faq">
							<div class="form-group">
								<label class="control-label mb-10">Judul:</label>
								<input type="hidden" class="form-control" id="idFaq_id" name="id">
								<input type="text" class="form-control" id="idFaq_judul" name="judul">
							</div>
							<div class="form-group">
								<label class="control-label mb-10">Deskripsi:</label>
								<textarea style="height:150px;" class="form-control" id="idFaq_deskripsi" name="deskripsi"></textarea>
							</div>
							<button class="btn btn-primary btn-block">Simpan</button>
						</form>
					</div>
				</div>
			</div>
		</div>
		
		
		<!-- MODAL TAMBAH PENGGUNA -->
		<div id="tambahPengguna" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
			<div class="modal-dialog">
				<div class="modal-content" style="height: auto;min-height: 100%;border-radius: 0;">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						<h5 class="modal-title">&nbsp;</h5>
					</div>
					<div class="modal-body">
						<form method="POST" action="<?=base_url();?>master/ins_faq">
							<div class="form-group">
								<label class="control-label mb-10">Judul:</label>
								<input type="text" class="form-control" name="judul" placeholder="Ketikan Judul">
							</div>
							<div class="form-group">
								<label class="control-label mb-10">Deskripsi:</label>
								<textarea style="height:150px;" class="form-control" name="deskripsi" placeholder="Ketikan Deskripsi"></textarea>
							</div>
							<button class="btn btn-primary btn-block">Simpan</button>
						</form>
					</div>
				</div>
			</div>
		</div>
		
		
	</div>
</div