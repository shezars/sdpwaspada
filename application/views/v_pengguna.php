<!-- Main Content -->
<div class="page-wrapper">
	<div class="container-fluid">
		<!-- Row -->
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default card-view">
					<div class="panel-wrapper collapse in">
						<div class="panel-body">
							<div class="table-wrap">
								<table id="footable_2" data-show-toggle="false" class="table" data-paging="true" data-filtering="true" data-sorting="true">
									<thead>
									<tr>
										<th data-type="html">&nbsp;</th>
										<th data-breakpoints="xs sm">&nbsp; </th>
									</tr>
									</thead>
									<tbody>
									<?php
									foreach($get_all_user as $row){
										// $temp_id	= $row['id'];
										// $temp_gc 	= $row['group_chat'];
										// $temp_cat 	= $row['category'];
										// $temp_all	= $temp_id.'#'.$temp_gc.'#'.$temp_cat;
										?>
										<tr>
											<td><img style="vertical-align:middle;" src="<?=$row['pp']==''?base_url('assets/img/default-avatar.png'):base_url('uploads/images/pp/'.$row['pp'].'');?>" width="10%" class="img-circle"> &nbsp;&nbsp; <?=$row['first_name'].$row['last_name'];?> 
												<?php
												if($row['approval_status']==0){
													?>
													<!-- <button onclick="approval_user(1)" style="max-width:30px;max-height:30px;" class="btn btn-primary btn-icon-anim btn-circle btn-xs pull-right"><i class="fa fa-times"></i></button>  -->
													<button onclick="approval_user(<?=$row['id'];?>)" style="max-width:30px;max-height:30px;" class="btn btn-primary btn-icon-anim btn-circle btn-xs pull-right"><i class="fa fa-check"></i></button>
													<?php
												}
												
												if($unit_current_user == 'administrator'){
													echo '<button style="max-width:30px;max-height:30px;" id="'.$row['id'].'" data-toggle="modal" data-target="#ubahPengguna" class="btn btn-primary btn-icon-anim btn-circle btn-xs pull-right ubahPengguna"><i class="fa fa-edit"></i></button>';
												}
												?>
											</td>
										</tr>
									<?php
									}
									?>
									</tbody>
								</table>
								<button class="btn btn-block btn-primary" data-toggle="modal" data-target="#tambahPengguna">TAMBAH PENGGUNA</button>
						</div>
					</div>
				</div>
				</div>
			</div>
		</div>
		<!-- /Row -->
		
		<!-- MODAL UBAH PENGGUNA -->
		<div id="ubahPengguna" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						<h5 class="modal-title">&nbsp;</h5>
					</div>
					<div class="modal-body">
						
						<div  class="pills-struct">
							<ul role="tablist" class="nav nav-pills" id="myTabs_6">
								<li class="active" role="presentation"><a aria-expanded="true"  data-toggle="tab" role="tab" id="home_tab_6" href="#home_6">Identitas</a></li>
								<!--
									<li role="presentation" class=""><a  data-toggle="tab" id="profile_tab_6" role="tab" href="#profile_6" aria-expanded="false">Akses</a></li>
								-->
							</ul>
							<div class="tab-content" id="myTabContent_6">
								<div  id="home_6" class="tab-pane fade active in" role="tabpanel">
									<form>
										<div class="form-group">
											<label class="control-label mb-10">Nama Lengkap:</label>
											<input type="text" class="form-control" id="idNamaLengkap">
										</div>
										<div class="form-group">
											<label class="control-label mb-10">Telepon:</label>
											<textarea class="form-control" id="idTelepon"></textarea>
										</div>
										<button class="btn btn-primary btn-block" disabled>Simpan</button>
									</form>
								</div>
								<input id="idCurrentUser" type="hidden">
								<!--
								<div  id="profile_6" class="tab-pane fade" role="tabpanel">
									Administrator Access :
									<br/>
									<input id="check_box_switch" type="checkbox" data-off-text="No" data-on-text="Yes"  class="bs-switch">
								</div>
								-->
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
		
		
		<!-- MODAL TAMBAH PENGGUNA -->
		<div id="tambahPengguna" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
			<div class="modal-dialog" style="width: 100%;height: 100%;margin: 0;padding: 10px;">
				<div class="modal-content" style="height: auto;min-height: 100%;border-radius: 0;">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						<h5 class="modal-title">&nbsp;</h5>
					</div>
					<div class="modal-body" style="height:500px;padding:0px;">
						<iframe src="<?=base_url();?>register/register_plain" frameborder="0" style="overflow:hidden;height:100%;width:100%" height="100%" width="100%">Your browser does not support iframes</iframe>
					</div>
				</div>
			</div>
		</div>
		
		
	</div>
</div