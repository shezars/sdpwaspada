<!-- Main Content -->
<div class="page-wrapper">
	<div class="container-fluid">
		<!-- Row -->
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default card-view">
					<div class="panel-wrapper collapse in">
						<div class="panel-body">
							<div class="table-wrap">
								<table id="footable_2" data-show-toggle="false" class="table" data-paging="true" data-filtering="true" data-sorting="false">
									<thead>
									<tr>
										<th data-type="html">&nbsp;</th>
										<th data-type="html">&nbsp;</th>
										<th data-type="html">&nbsp;</th>
										<th data-breakpoints="xs sm">&nbsp; </th>
									</tr>
									</thead>
									<tbody>
									<?php
									foreach($get_all_grup as $row){
										// $temp_id		= $row['id'];
										// $temp_gc 	= $row['group_chat'];
										// $temp_cat 	= $row['category'];
										// $temp_all	= $temp_id.'#'.$temp_gc.'#'.$temp_cat;
										?>
										<tr>
											<td width="20%">
												<img align="left" style="margin-right:5px;" src="<?=$row['pp']==''?base_url('assets/img/default-avatar.png'):base_url('uploads/images/pp/'.$row['pp'].'');?>" width="100%" class="img-circle">
											</td>
											<td> <?=$row['name'];?> 
												
											</td>
											<td>
											<?php
											
												echo '<button style="max-width:30px;max-height:30px;" id="'.$row['id'].'" data-toggle="modal" data-target="#ubahGrup" class="btn btn-primary btn-icon-anim btn-circle btn-xs pull-right ubahGrup"><i class="fa fa-edit"></i></button>
												<button style="max-width:30px;max-height:30px;" id="'.$row['id'].'" data-toggle="modal" data-target="#aturAnggota" class="btn btn-danger btn-icon-anim btn-circle btn-xs pull-right aturAnggota"><i class="fa fa-users"></i></button>';
												
												?>
											</td>
										</tr>
									<?php
									}
									?>
									</tbody>
								</table>
								<button class="btn btn-block btn-primary" data-toggle="modal" data-target="#tambahGrup">TAMBAH GRUP</button>
						</div>
					</div>
				</div>
				</div>
			</div>
		</div>
		<!-- /Row -->
		
		<!-- MODAL UBAH PENGGUNA -->
		<div id="ubahGrup" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						<h5 class="modal-title">&nbsp;</h5>
					</div>
					<div class="modal-body">
						
						<form action="<?=base_url();?>master/edit_grup" method="POST">
							<div class="form-group">
								<label class="control-label mb-10">Unit:</label>
								<select class="form-control" id="idMaster_unit" name="unit_grup">
									<option>--pilih unit--</option>
									<option value="ditjenpas">Ditjenpas</option>
									<option value="kanwil">Kanwil</option>
									<option value="upt">UPT</option>
								</select>
							</div>
							<div class="form-group">
								<label class="control-label mb-10">Nama Grup:</label>
								<input type="text" class="form-control" id="idMaster_NamaGrup" name="nama_grup">
								<input type="hidden" class="form-control" id="idMaster_IdGrup" name="id_grup">
							</div>
							<button class="btn btn-primary btn-block">Simpan</button>
						</form>
						
					</div>
				</div>
			</div>
		</div>
		
		
		<!-- MODAL TAMBAH PENGGUNA -->
		<div id="tambahGrup" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
			<div class="modal-dialog" style="width: 100%;height: 100%;margin: 0;padding: 10px;">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						<h5 class="modal-title">&nbsp;</h5>
					</div>
					<div class="modal-body">
						<form action="<?=base_url();?>master/add_grup" method="POST">
							<!--
							<div class="form-group" align="center">
								
								<div class="image-upload">
								  <label for="file-input">
									<img src="<?=base_url();?>assets/img/default-avatar.png" width="50%"/>
								  </label>

								  <input id="file-input" type="file" style="display: none;" />
								</div>
							</div>
							-->
							<div class="form-group">
								<label class="control-label mb-10">Unit:</label>
								<select name="unit_grup" class="form-control">
									<option>--pilih unit--</option>
									<option>Ditjenpas</option>
									<option>Kanwil</option>
									<option>UPT</option>
								</select>
							</div>
							<div class="form-group">
								<label class="control-label mb-10">Nama Grup:</label>
								<input type="text" class="form-control" id="idNamaGrup" name="nama_grup">
							</div>
							<button class="btn btn-primary btn-block">Simpan</button>
						</form>
					</div>
				</div>
			</div>
		</div>
		
		<!-- MODAL TAMBAH PENGGUNA -->
		<div id="aturAnggota" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
			<div class="modal-dialog" style="width: 100%;height: 100%;margin: 0;padding: 10px;">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						<h5 class="modal-title">&nbsp;</h5>
					</div>
					<div class="modal-body">
						<div id="idBody01">
							<table id="footable_22" data-show-toggle="false" class="table" data-paging="true" data-filtering="true" data-sorting="false">
								<thead>
								<tr>
									<th data-type="html">&nbsp;</th>
									<th data-type="html">&nbsp;</th>
									<th data-type="html">&nbsp;</th>
									<th data-breakpoints="xs sm">&nbsp; </th>
								</tr>
								</thead>
								<input type="hidden" id="idModalTambahAnggota_group">
								<tbody id="idMasterGrup_tbody">
									
									
									<!--
									<tr>
										<td width="20%">
											<img align="left" style="margin-right:5px;" src="<?=base_url('assets/img/default-avatar.png');?>" width="100%" class="img-circle">
										</td>
										<td>ABCD</td>
										<td>
										<?php
											echo '<button style="max-width:30px;max-height:30px;" id="1" data-toggle="modal" data-target="#ubahGrup" class="btn btn-primary btn-icon-anim btn-circle btn-xs pull-right ubahGrup"><i class="fa fa-edit"></i></button>
											<button style="max-width:30px;max-height:30px;" id="2" data-toggle="modal" data-target="#aturAnggota" class="btn btn-danger btn-icon-anim btn-circle btn-xs pull-right aturAnggota"><i class="fa fa-users"></i></button>';
											?>
										</td>
									</tr>
									-->
								</tbody>
							</table>
							<button class="btn btn-block btn-primary" id="btnTambahAnggota">TAMBAH ANGGOTA</button>
						</div>
						<div id="idBody02" class="hide">
							<h3>TAMBAH ANGGOTA</h3>
							<table id="footable_228" data-show-toggle="false" class="table" data-paging="true" data-filtering="true" data-sorting="false">
								<thead>
								<tr>
									<th data-type="html">&nbsp;</th>
									<th data-type="html">&nbsp;</th>
									<th data-type="html">&nbsp;</th>
									<th data-breakpoints="xs sm">&nbsp; </th>
								</tr>
								</thead>
								<tbody id="idMasterGrupTambahAnggota_tbody">
									
									
									<!--
									<tr>
										<td width="20%">
											<img align="left" style="margin-right:5px;" src="<?=base_url('assets/img/default-avatar.png');?>" width="100%" class="img-circle">
										</td>
										<td>ABCD</td>
										<td>
										<?php
											echo '<button style="max-width:30px;max-height:30px;" id="1" data-toggle="modal" data-target="#ubahGrup" class="btn btn-primary btn-icon-anim btn-circle btn-xs pull-right ubahGrup"><i class="fa fa-edit"></i></button>
											<button style="max-width:30px;max-height:30px;" id="2" data-toggle="modal" data-target="#aturAnggota" class="btn btn-danger btn-icon-anim btn-circle btn-xs pull-right aturAnggota"><i class="fa fa-users"></i></button>';
											?>
										</td>
									</tr>
									-->
								</tbody>
							</table>
							<button class="btn btn-block btn-warning" id="btnKembaliTambahAnggota">< Kembali</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		
	</div>
</div