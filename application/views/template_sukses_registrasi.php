<html>
<head>
<style>
body{
	background-color: #F0F0F0;
}
table{ 
	margin-left:auto;
	margin-right:auto;
	/*
	width:50%;
	*/
	background-color:#FFF;
	border-radius:1rem;
	padding:1rem;
}
tr.border_bottom td {
	border-bottom:1pt solid black;
	line-height:2rem;
}
tr.height{
	line-height:2rem;
}
</style>
</head>
<body style="font-family:tahoma, serif;">
<table border=0>
	<tr>
		<!--<td colspan=2 align="center"><img src="<?=base_url();?>assets/img/logo.png"><hr/></td>-->
		<td colspan=2 align="center"><img src="http://tinyimg.io/i/Voe56xG.png"><hr/></td>
	</tr>
	
	<tr class="height">
		<td colspan=2 style="font-size:14pt;">Halo, <?=$nama_lengkap;?></td>
	</tr>
	
	<tr>
		<td colspan=2>Terima kasih, data anda sudah masuk kedalam sistem</td>
	</tr>
	
	<tr>
		<td colspan=2>&nbsp;</td>
	</tr>
	
	<tr style="padding-top:2rem;" class="border_bottom">
		<td width="20%;">Nama</td><td>: <?=$nama_lengkap;?></td>
	</tr>
	<tr class="border_bottom">
		<td>Jabatan</td><td>: <?=$jabatan;?></td>
	</tr>
	<tr class="border_bottom">
		<td>Username</td><td>: <?=$username;?></td>
	</tr>
	<tr class="border_bottom">
		<td>Password</td><td>: <?=$password;?></td>
	</tr>
	<tr class="border_bottom">
		<td>Status</td><td>: <?=$status;?></td>
	</tr>
	<tr>
		<td colspan=2>&nbsp;</td>
	</tr>
	<tr>
		<td colspan=2 style="font-size:10pt;" align="center">Email ini dibuat secara otomatis. Mohon tidak mengirimkan balasan ke email ini.</td>
	</tr>
	
</table>

</body>
</html>