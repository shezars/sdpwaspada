<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<title>SDP Waspada</title>
		<meta name="description" content="Grandin is a Dashboard & Admin Site Responsive Template by hencework." />
		<meta name="keywords" content="admin, admin dashboard, admin template, cms, crm, Grandin Admin, Grandinadmin, premium admin templates, responsive admin, sass, panel, software, ui, visualization, web app, application" />
		<meta name="author" content="hencework"/>
		
		<!-- Favicon -->
		<link rel="shortcut icon" href="favicon.ico">
		<link rel="icon" href="favicon.ico" type="image/x-icon">
		
		
		<!-- CSS Files -->
		<link href="<?=base_url();?>assets/register/css/bootstrap.min.css" rel="stylesheet" />
		<link href="<?=base_url();?>assets/register/css/paper-bootstrap-wizard.css" rel="stylesheet" />
		<link href="https://netdna.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.css" rel="stylesheet">
		<link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
		<link href="<?=base_url();?>assets/register/css/themify-icons.css" rel="stylesheet">
		
		<!-- JQuery Typeahead -->
		<link href="<?=base_url();?>assets/dist/jquerytypeahead/jquery.typeahead.min.css" rel="stylesheet" type="text/css"/>
	
	
		<style>
		.disabledTab {
			pointer-events: none;
		}
		</style>
	</head>
	<body style="background-color:#212121;">
		<!--Preloader-->
		<div class="preloader-it">
			<div class="la-anim-1"></div>
		</div>
		<!--/Preloader-->	
			<div align="center" style="padding-top:2rem;">
				<a href="<?=base_url();?>"><img class="brand-img mr-10" src="<?=base_url();?>assets/img/logo.png" alt="brand"/></a>
			</div>
			<!-- Main Content -->
			<div style="padding-top:2rem;" class="page-wrapper pa-0 ma-0 auth-page">
				<div class="container-fluid">


		                <div class="card wizard-card" data-color="orange" id="wizardProfile">
		                    <form action="" method="">
		                <!--        You can switch " data-color="orange" "  with one of the next bright colors: "blue", "green", "orange", "red", "azure"          -->


								<div class="wizard-navigation">
									<div class="progress-with-circle">
									     <div class="progress-bar" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="3" style="width: 21%;"></div>
									</div>
									<ul>
			                            <li class="disabledTab">
											<a href="#about" data-toggle="tab">
												<div class="icon-circle">
													<i class="ti-user"></i>
												</div>
												Informasi Diri
											</a>
										</li>
										<li class="disabledTab">
											<a href="#about2" data-toggle="tab">
												<div class="icon-circle">
													<i class="ti-user"></i>
												</div>
												Informasi Akun
											</a>
										</li>
			                            <li class="disabledTab">
											<a href="#account" data-toggle="tab">
												<div class="icon-circle">
													<i class="ti-settings"></i>
												</div>
												Unit
											</a>
										</li>
			                            <li class="disabledTab">
											<a href="#address" data-toggle="tab">
												<div class="icon-circle">
													<i class="ti-map"></i>
												</div>
												Satker
											</a>
										</li>
			                        </ul>
								</div>
		                        <div class="tab-content">
		                            <div class="tab-pane" id="about" style="padding-top:4rem;">
		                            	<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label>Nama Lengkap <small>(required)</small></label>
													<input name="firstname" id="id_nama_lengkap" type="text" class="form-control" placeholder="Isikan Nama Lengkap">
												</div>
												<div class="form-group">
													<label>Jenis Kelamin <small>(required)</small></label>
													<br/>
													<input type="radio" name="jekel" class="jekel" value="laki-laki"> Laki-laki<br/>
													<input type="radio" name="jekel" class="jekel" value="perempuan"> Perempuan
												</div>												
												<div class="form-group"> 	
													<label>No. HP <small>(required)</small></label>
													<input name="No HP" id="id_no_hp" type="number" class="form-control" placeholder="Isikan No. HP">
												</div>
											</div>
										</div>
		                            </div>
									<div class="tab-pane" id="about2" style="padding-top:4rem;">
		                            	<div class="row">
											<div class="col-sm-10 col-sm-offset-1">
												<div class="form-group">
													<label>NIP <small>(required)</small></label>
													<input name="NIP" id="id_nip" required type="number" class="form-control" placeholder="Isikan NIP">
												</div>
												<div class="form-group">
													<label>Email <small>(required)</small></label>
													<input name="email" id="id_email" type="email" class="form-control" placeholder="Isikan E-Mail">
												</div>
												<div class="form-group"> 	
													<label>Password <small>(required)</small></label>
													<input name="No HP" required id="id_password" type="password" class="form-control" placeholder="Isikan Password">
												</div>
											</div>
										</div>
		                            </div>
		                            <div class="tab-pane" id="account">
		                                <div class="row">
											<input type="hidden" id="id_unit">
		                                    <div class="col-sm-8 col-sm-offset-2">
		                                        <div class="col-sm-4">
		                                            <div class="choice" id="idChoice_ditjenpas" data-id="ditjenpas" data-toggle="wizard-checkbox">
		                                                <div class="card card-checkboxes card-hover-effect kodok">
		                                                    <i class="ti-home"></i>
															<p>DITJENPAS</p>
		                                                </div>
		                                            </div>
		                                        </div>
		                                        <div class="col-sm-4">
		                                            <div class="choice" id="idChoice_kanwil" data-id="kanwil" data-toggle="wizard-checkbox">
		                                                <div class="card card-checkboxes card-hover-effect kodok">
		                                                    <i class="ti-home"></i>
															<p>KANWIL</p>
		                                                </div>
		                                            </div>
		                                        </div>
		                                        <div class="col-sm-4">
		                                            <div class="choice" id="idChoice_upt" data-id="upt" data-toggle="wizard-checkbox">
		                                                <div class="card card-checkboxes card-hover-effect kodok">
		                                                    <i class="ti-home"></i>
															<p>UPT</p>
		                                                </div>
		                                            </div>
		                                        </div>
		                                    </div>
		                                </div>
		                            </div>
		                            <div class="tab-pane" id="address" style="padding-top:2rem;">
		                                <div class="row" id="idFormRegister">
		                                    <div class="col-sm-5">
		                                        <div class="form-group">
		                                            <label>Pilih Direktorat</label><br>
		                                            <select name="direktorat" id="idDirektorat" class="form-control">
														<option selected="true" disabled="disabled">-- pilih direktorat --</option>
														<?php
														foreach($direktorat as $row){
															?>
															<option value="<?=$row['id_struktur'];?>"><?=$row['nm_struktur'];?></option>
															<?php
														}
														?>
		                                            </select>
		                                        </div>
		                                    </div>
											<div class="col-sm-5 hide" id="idAreaSubDirektorat">
		                                        <div class="form-group">
		                                            <label>Pilih Subdirektorat</label><br>
		                                            <select name="subdirektorat" id="idSubDirektorat" class="form-control"></select>
		                                        </div>
		                                    </div>
											<div class="col-sm-5 hide" id="idAreaJabatan">
		                                        <div class="form-group">
		                                            <label>Pilih Jabatan</label><br>
		                                            <select name="subdirektorat" id="idJabatan" class="form-control"></select>
		                                        </div>
		                                    </div>
		                                </div>
										<div class="row hide" id="idFormRegister2">
		                                    <div class="col-sm-5" id="idKanwil_register2">
		                                        <div class="form-group">
		                                            <label>Kanwil</label><br>
													<div class="typeahead__container">
														<div class="typeahead__field">
															<div class="typeahead__query">
																<input class="js-typeahead-car_v1" id="idKanwil_Register" name="car_v1[query]" type="search" placeholder="Search" autocomplete="off">
															</div>
														</div>
													</div>	                                            
		                                        </div>
		                                    </div>
											<div class="col-sm-5 hide" id="idAreaJabatan_register2">
		                                        <div class="form-group">
		                                            <label>Pilih Jabatan</label><br>
		                                            <select name="subdirektorat" id="idKanwilJabatan" class="form-control">
													<option selected="true" disabled="disabled">-- pilih jabatan --</option>
													<?php
													foreach($kanwil_jabatan as $row){
														?>
														<option value="<?=$row['id_struktur_jabatan'];?>"><?=$row['nm_jabatan'];?></option>
														<?php
													}
													?>
													</select>
		                                        </div>
		                                    </div>
		                                </div>
										<div class="row hide" id="idFormRegister3">
		                                    <div class="col-sm-5">
												<div class="form-group">
		                                            <label>Kanwil</label><br>
													<div class="typeahead__container">
														<div class="typeahead__field">
															<div class="typeahead__query">
																<input class="js-typeahead-car_v1" id="idUpt_Register" type="search" placeholder="Search" autocomplete="off">
															</div>
														</div>
													</div>	                                            
		                                        </div>
											</div>
											<div class="col-sm-5 hide" id="idAreaUpt">
												<div class="form-group">
													<label>Pilih UPT</label><br>
													<select name="subdirektorat" id="idUptKanwil" class="form-control"></select>
												</div>
											</div>
											<div class="col-sm-5 hide" id="idAreaJabatan_register3">
												<div class="form-group">
													<label>Pilih Jabatan</label><br>
													<select name="subdirektorat" id="idUptJabatan" class="form-control">
													<option selected="true" disabled="disabled">-- pilih jabatan --</option>
													<?php
													foreach($upt_jabatan as $row){
														?>
														<option value="<?=$row['id_struktur_jabatan'];?>"><?=$row['nm_jabatan'];?></option>
														<?php
													}
													?>
													</select>
												</div>
											</div>
		                                </div>
		                            </div>
		                        </div>
		                        <div class="wizard-footer">
		                            <div class="pull-right">
		                                <input type='button' class='btn btn-next btn-fill btn-warning btn-wd' id="btnNext" name='next' value='Next' />
		                                <input type='button' class='btn btn-finish btn-fill btn-warning btn-wd finish' name='finish' value='Submit' />
		                            </div>

		                            <div class="pull-left">
		                                <input type='button' class='btn btn-previous btn-default btn-wd' name='previous' value='Previous' />
		                            </div>
		                            <div class="clearfix"></div>
		                        </div>
		                    </form>
		                </div>
		            
				</div>
				
			</div>
			<!-- /Main Content -->
		
		
		<!--   Core JS Files   -->
		<script src="<?=base_url();?>assets/register/js/jquery-2.2.4.min.js" type="text/javascript"></script>
		<script src="<?=base_url();?>assets/register/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="<?=base_url();?>assets/register/js/jquery.bootstrap.wizard.js" type="text/javascript"></script>
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

		<!--  Plugin for the Wizard -->
		<script src="<?=base_url();?>assets/register/js/demo.js" type="text/javascript"></script>
		<script src="<?=base_url();?>assets/register/js/paper-bootstrap-wizard.js" type="text/javascript"></script>

		<!--  More information about jquery.validate here: https://jqueryvalidation.org/	 -->
		<script src="<?=base_url();?>assets/register/js/jquery.validate.min.js" type="text/javascript"></script>
		
		<!-- Select2 JavaScript -->
		<script src="<?=base_url();?>assets/dist/jquerytypeahead/jquery.typeahead.min.js"></script>
		
		<script>		
		$("#idDirektorat").change(function(){
			var id = this.id;
			id = $("#"+id).val();
			$.post('<?=base_url();?>register/get_subdirektorat',{id:id}).done(function(data){
				var obj = jQuery.parseJSON(data);
				$("#idSubDirektorat").html($("<option></option>").attr("value","").text(""));
				$("#idSubDirektorat").html($("<option selected='true' disabled='disabled'></option>").attr("value","").text("-- pilih subdirektorat --"));
				$.each(obj,function(key,value){
					$("#idSubDirektorat").append($("<option></option>").attr("value",value.id_struktur).text(value.nm_struktur));
				})
				$("#idAreaSubDirektorat").removeClass('hide');
			})
		})
		$("#idSubDirektorat").change(function(){
			var id = this.id;
			id = $("#"+id).val();
			$.post('<?=base_url();?>register/get_jabatan',{id:id}).done(function(data){
				var obj = jQuery.parseJSON(data);
				
				$("#idJabatan").html($("<option></option>").attr("value","").text(""));
				$("#idJabatan").html($("<option selected='true' disabled='disabled'></option>").attr("value","").text("-- pilih jabatan --"));
				$.each(obj,function(key,value){
					$("#idJabatan").append($("<option></option>").attr("value",value.id_struktur_jabatan).text(value.nm_jabatan));
				})
				$("#idAreaJabatan").removeClass('hide');
			})
		})
		$('.choice').click(function() {
			$("#id_unit").val($(this).data("id"));
			
			if(this.id=='idChoice_ditjenpas'){
				
				if(!$("#idChoice_ditjenpas").hasClass("active")){
					$("#idChoice_kanwil").removeClass("active");
					$("#idChoice_upt").removeClass("active");
				}
				
				$("#idFormRegister").removeClass("hide");
				$("#idFormRegister2").addClass("hide");
				$("#idFormRegister3").addClass("hide");
				
			}else if(this.id=='idChoice_kanwil'){
				if(!$("#idChoice_kanwil").hasClass("active")){
					$("#idChoice_ditjenpas").removeClass("active");
					$("#idChoice_upt").removeClass("active");
				}
				
				$("#idFormRegister").addClass("hide");
				$("#idFormRegister2").removeClass("hide");
				$("#idFormRegister3").addClass("hide");
			}else{
				if(!$("#idChoice_upt").hasClass("active")){
					$("#idChoice_ditjenpas").removeClass("active");
					$("#idChoice_kanwil").removeClass("active");
				}
				
				$("#idFormRegister").addClass("hide");
				$("#idFormRegister2").addClass("hide");
				$("#idFormRegister3").removeClass("hide");
			}
		});
		$(".finish").click(function(){
			var unit 						= $("#id_unit").val();
			var arr 						= {};
			
			arr['nama_lengkap']				= $("#id_nama_lengkap").val();
			arr['jekel']					= $("input:radio.jekel:checked").val();
			arr['nip']						= $("#id_nip").val();
			arr['no_hp']					= $("#id_no_hp").val();
			arr['email']					= $("#id_email").val();
			arr['password']					= $("#id_password").val();
			arr['unit']						= $("#id_unit").val();
				
			if(unit == "ditjenpas"){
				arr['direktorat']	= $("#idDirektorat").val();
				arr['sub_direktorat']= $("#idSubDirektorat").val();
				arr['jabatan']		= $("#idJabatan").val();
			}else if(unit == "kanwil"){
				arr['kanwil']				= $("#idKanwil_Register").val();
				arr['jabatan']		= $("#idKanwilJabatan").val();
			}else{
				arr['kanwil']				= $("#idUpt_Register").val();
				arr['upt']					= $("#idUptKanwil").val();
				arr['jabatan']				= $("#idUptJabatan").val();
			}
			
			$.post('<?=base_url();?>register/do_register',arr).done(function(data){
				swal("Registrasi Berhasil", "Harap menunggu verifikasi", "success");
				setTimeout(window.location.href = '<?=base_url();?>' , 3000);
			});
			
		})
		
		function contoh(){
			$.post('<?=base_url();?>register/get_kanwil_uraian').done(function(data){
				return data;
			})
		}
		
		$.typeahead({
			input: '.js-typeahead-car_v1',
			minLength: 1,
			order: "asc",
			offset: false,
			hint: true,
			source: {
				car: {
					data: ["My first added brand", "M1 added brand at start"],
					ajax: {
						type: "POST",
						url: "<?=base_url();?>register/get_kanwil_uraian",
						data: {
							myKey: "myValue"
						}
					}
				}
			},
			callback: {
				onClick: function (node, a, item, event) {
					if(node[0].id == 'idKanwil_Register'){
						$("#idAreaJabatan_register2").removeClass("hide");
					}else{
						var kanwil = item.display;
						$.post('<?=base_url();?>register/get_upt_by_kanwil',{kanwil:kanwil}).done(function(data){
							var obj = jQuery.parseJSON(data);
							$("#idUptKanwil").html($("<option></option>").attr("value","0").text("-- Pilih UPT --"));
							$.each(obj,function(key,value){
								$("#idUptKanwil").append($("<option></option>").attr("value",value.id_upt).text(value.uraian));
							});
							$("#idAreaUpt").removeClass("hide");
						})
					}
				}
			}
		});
		
		$('#idUptKanwil').change(function(){
			$("#idAreaJabatan_register3").removeClass("hide");
		})
		
		$("#id_nip").focusout(function(){
			var nip = $("#id_nip").val();
			$.post('<?=base_url();?>register/check_nip',{nip:nip}).done(function(data){
				if(data==1){
					$("#btnNext").prop("disabled",true);
					$("#id_email").prop("disabled",true);
					$("#id_password").prop("disabled",true);
					swal("NIP Sudah Digunakan", "Silakan Gunakan NIP Lain / Hubungi Admin", "error");
				}else{
					$("#btnNext").prop("disabled",false);
					$("#id_email").prop("disabled",false);
					$("#id_password").prop("disabled",false);
				}
			})
		})
		
		</script>
	
	</body>
</html>
