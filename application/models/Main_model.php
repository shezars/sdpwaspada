<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main_model extends CI_Model {
	
	function group_chat_user($id_user){
		$dataPersonal		= $this->db->get_where('users',array('id'=>$id_user))->row();
		$id_dataPersonal	= $dataPersonal->id_jabatan;
		
		$dataTemp = null;
		// echo $id_user;die;
		$sqlCoba =  "
						SELECT gcu.id, gcu.id_group, gcu.status, gcu.id_jabatan, gcu.category,gcu.user FROM group_chat_user gcu
						WHERE 
						gcu.user LIKE '%$id_user' OR
						gcu.user LIKE '%,$id_user' OR
						gcu.user LIKE '%,$id_user,%' OR
						gcu.user LIKE '$id_user,%'
					";
		$checkSqlCoba = $this->db->query($sqlCoba)->result_array();
		$checkSqlCoba2 = Array();
		
		$x=0;
		foreach($checkSqlCoba as $key => $row){
			$group_chat=null;
			$id_jabatan = $row['id_jabatan'];
			if($row['category']=='personal' AND $row['status']==1){
				$checkSqlCoba2[$x] = $row;
				
				$sql = "SELECT CASE WHEN POSITION('$id_user' IN g.user)=1
						THEN
							1
						ELSE
							2
						END
						FROM group_chat_user g WHERE id=".$row['id']."
				";
				$resultSql = $this->db->query($sql)->row()->case;
				if($resultSql==1){
					$sqlFull = "SELECT
						 REPLACE(g.user,'$id_user,','') as id_dest 
						 FROM group_chat_user g
						 WHERE
							g.id = ".$row['id']."
						 ";
				}else{
					$sqlFull = "SELECT
					 REPLACE(g.user,',$id_user','') as id_dest 
					 FROM group_chat_user g
						 WHERE
							g.id = ".$row['id']."
					 ";
				}
				
				$id_dest = $this->db->query($sqlFull)->row()->id_dest;
				$group_chat=$this->db->query("SELECT get_user_detail('$id_dest')")->row()->get_user_detail;
				$checkSqlCoba2[$x]['group_chat'] = $group_chat;
				
			}else{
				if($row['status']==1 && $row['id_group']!=null){
					$checkSqlCoba2[$x] = $row;
					
					$group_data	= $this->get_grup($row['id_group']);
					$checkSqlCoba2[$x]['group_chat'] = $group_data->name;
				}else if($row['status']==1){
					$checkSqlCoba2[$x] = $row;
					
					$group_chat = $this->db->query("SELECT get_group_detail_default('$id_jabatan')")->row()->get_group_detail_default;
					$checkSqlCoba2[$x]['group_chat'] = $group_chat;
				}
			}
			$x++;
		}
		
		return $checkSqlCoba2;
	}
	function group_chat_user_kontak($id_user,$param=null){
		$dataPersonal		= $this->db->get_where('users',array('id'=>$id_user))->row();
		
		$unit_dataPersonal	= $dataPersonal->unit;
		$id_dataPersonal	= $dataPersonal->id_jabatan;
		
		$data_users 		= null;
		if($param=='administrator2'){
			$sql = "SELECT u.id,u.approval_status,u.first_name,u.last_name,u.pp FROM users u WHERE u.id <> ".$id_user." AND u.approval_status=1";
			$sql_proses 	= $this->db->query($sql)->result_array(); 
			$x=0;
			foreach($sql_proses as $row){
				$xyz	= 0;
				$abc	= 0;
				
				$sql 	= $this->db->query("SELECT x.id FROM group_chat_user x WHERE x.user = '".$id_user.",".$row['id']."' AND x.category='personal' AND x.unit='ditjenpas'")->row();
				$sql2 	= $this->db->query("SELECT x.id FROM group_chat_user x WHERE x.user = '".$row['id'].",".$id_user."' AND x.category='personal' AND x.unit='ditjenpas'")->row();
				if(!empty($sql->id)){
					$xyz=1;
					$abc=$sql->id;
				}
				if(!empty($sql2->id)){
					$xyz=1;
					$abc=$sql2->id;
				}
				
				$data_users[$x]['id']					= $row['id'];
				$data_users[$x]['id_gcu']				= $abc;
				$data_users[$x]['approval_status']		= $row['approval_status'];
				$data_users[$x]['first_name']			= $row['first_name'];
				$data_users[$x]['last_name']			= $row['last_name'];
				$data_users[$x]['pp']					= $row['pp'];
				$x++;
			}
		}else if($unit_dataPersonal=='administrator'){
			$sql = "SELECT u.id,u.approval_status,u.first_name,u.last_name,u.pp FROM users u WHERE u.id <> ".$id_user." AND u.approval_status=0";
			$sql_proses 	= $this->db->query($sql)->result_array(); 
			$x=0;
			foreach($sql_proses as $row){
				$xyz	= 0;
				$abc	= 0;
				
				$sql 	= $this->db->query("SELECT x.id FROM group_chat_user x WHERE x.user = '".$id_user.",".$row['id']."' AND x.category='personal' AND x.unit='ditjenpas'")->row();
				$sql2 	= $this->db->query("SELECT x.id FROM group_chat_user x WHERE x.user = '".$row['id'].",".$id_user."' AND x.category='personal' AND x.unit='ditjenpas'")->row();
				if(!empty($sql->id)){
					$xyz=1;
					$abc=$sql->id;
				}
				if(!empty($sql2->id)){
					$xyz=1;
					$abc=$sql2->id;
				}
				
				$data_users[$x]['id']					= $row['id'];
				$data_users[$x]['id_gcu']				= $abc;
				$data_users[$x]['approval_status']		= $row['approval_status'];
				$data_users[$x]['first_name']			= $row['first_name'];
				$data_users[$x]['last_name']			= $row['last_name'];
				$data_users[$x]['pp']					= $row['pp'];
				$x++;
			}
			
		}else if($unit_dataPersonal=='ditjenpas'){
			$sql = "SELECT u.id,u.approval_status,u.first_name,u.last_name,u.pp FROM users u WHERE u.unit = 'ditjenpas' AND u.id <> ".$id_user."";
			$sql_proses 	= $this->db->query($sql)->result_array(); 
			$x=0;
			foreach($sql_proses as $row){
				$xyz	= 0;
				$abc	= 0;
				
				$sql 	= $this->db->query("SELECT x.id FROM group_chat_user x WHERE x.user = '".$id_user.",".$row['id']."' AND x.category='personal' AND x.unit='ditjenpas'")->row();
				$sql2 	= $this->db->query("SELECT x.id FROM group_chat_user x WHERE x.user = '".$row['id'].",".$id_user."' AND x.category='personal' AND x.unit='ditjenpas'")->row();
				if(!empty($sql->id)){
					$xyz=1;
					$abc=$sql->id;
				}
				if(!empty($sql2->id)){
					$xyz=1;
					$abc=$sql2->id;
				}
				
				$data_users[$x]['id']					= $row['id'];
				$data_users[$x]['id_gcu']				= $abc;
				$data_users[$x]['approval_status']		= $row['approval_status'];
				$data_users[$x]['first_name']			= $row['first_name'];
				$data_users[$x]['last_name']			= $row['last_name'];
				$data_users[$x]['pp']					= $row['pp'];
				$x++;
			}
		}else if($unit_dataPersonal=='kanwil'){
			//KANWIL
			$data_users_personal = $this->db->query("SELECT u.id,u.approval_status,u.first_name,u.last_name,u.pp FROM users u WHERE u.unit = 'kanwil' AND u.id <> ".$id_user."")->result_array(); 
			$x=0;
			foreach($data_users_personal as $row){
				$xyz	= 0;
				$abc	= 0;
				
				$sql 	= $this->db->query("SELECT x.id FROM group_chat_user x WHERE x.user = '".$id_user.",".$row['id']."' AND x.category='personal' AND x.unit='kanwil'")->row();
				$sql2 	= $this->db->query("SELECT x.id FROM group_chat_user x WHERE x.user = '".$row['id'].",".$id_user."' AND x.category='personal' AND x.unit='kanwil'")->row();
				if(!empty($sql->id)){
					$xyz=1;
					$abc=$sql->id;
				}
				if(!empty($sql2->id)){
					$xyz=1;
					$abc=$sql2->id;
				}
				
				$data_users[$x]['id']					= $row['id'];
				$data_users[$x]['id_gcu']				= $abc;
				$data_users[$x]['approval_status']		= $row['approval_status'];
				$data_users[$x]['first_name']			= $row['first_name'];
				$data_users[$x]['last_name']			= $row['last_name'];
				$data_users[$x]['pp']					= $row['pp'];
				$x++;
			}
		}else{
			//UPT
			$data_users_personal = $this->db->query("SELECT u.id,u.approval_status,u.first_name,u.last_name,u.pp FROM users u WHERE u.unit = 'upt' AND u.id <> ".$id_user."")->result_array(); 
			$x=0;
			foreach($data_users_personal as $row){
				$xyz	= 0;
				$abc	= 0;
				
				$sql 	= $this->db->query("SELECT x.id FROM group_chat_user x WHERE x.user = '".$id_user.",".$row['id']."' AND x.category='personal' AND x.unit='upt'")->row();
				$sql2 	= $this->db->query("SELECT x.id FROM group_chat_user x WHERE x.user = '".$row['id'].",".$id_user."' AND x.category='personal' AND x.unit='upt'")->row();
				if(!empty($sql->id)){
					$xyz=1;
					$abc=$sql->id;
				}
				if(!empty($sql2->id)){
					$xyz=1;
					$abc=$sql2->id;
				}
				
				$data_users[$x]['id']					= $row['id'];
				$data_users[$x]['id_gcu']				= $abc;
				$data_users[$x]['approval_status']		= $row['approval_status'];
				$data_users[$x]['first_name']			= $row['first_name'];
				$data_users[$x]['last_name']			= $row['last_name'];
				$data_users[$x]['pp']					= $row['pp'];
				$x++;
			}
		}
		
		return $data_users;die;
	}
	function get_user($id){
		$this->db->where('id',$id);
		return $this->db->get('users')->row();
	}
	function get_user_by_nip($nip){
		$this->db->where('username',$nip);
		return $this->db->get('users')->row();
	}
	function get_all_anggota(){
		$this->db->where('level <>','2');
		$this->db->where('unit <>','administrator');
		return $this->db->get('users')->result_array();
	}
	function get_faq_detail($id){
		$this->db->where('id',$id);
		return $this->db->get('faq')->row();
	}
	function get_all_grup(){
		$this->db->where('unit <>','system');
		$this->db->where('type','group');
		return $this->db->get('groups')->result_array();
	}
	function add_grup($data){
		$this->db->insert('groups',$data);
	}
	function get_grup($id){
		$this->db->where('id',$id);
		return $this->db->get('groups')->row();
	}
	function edit_grup($data,$id_grup){
		$this->db->where('id',$id_grup);
		$this->db->update('groups',$data);
	}
	function get_all_faq(){
		$this->db->order_by('id','asc');
		return $this->db->get('faq')->result_array();
	}
	function get_all_user(){
		$this->db->where('approval_status','1');
		return $this->db->get('users')->result_array();
	}
	function upd_pengguna($data,$id){
		$this->db->where('id',$id);
		$this->db->update('users',$data);
	}
	function ins_faq($data){
		$this->db->insert('faq',$data);
	}
	function del_faq($id){
		$this->db->where('id',$id);
		$this->db->delete('faq');
	}
	function upd_faq($id,$data){
		$this->db->where('id',$id);
		$this->db->update('faq',$data);
	}
	function check_gcu_personal($idRegistrant,$idMember){
		$this->db->where('category','personal');
		$this->db->where('user',$idRegistrant.','.$idMember);
		$this->db->or_where('user',$idMember.','.$idRegistrant);
		return $this->db->get('group_chat_user')->num_rows();
	}
	function check_gcu($idGroup){
		$this->db->where('id_group',$idGroup);
		return $this->db->get('group_chat_user')->row();
	}
	function insert_gcu_personal($data){
		$this->db->insert('group_chat_user',$data);
	}
	function update_pp($id_user,$file_name){
		$data = array(
			'pp' => $file_name
		);
		$this->db->where('id',$id_user);
		$this->db->update('users',$data);
	}
	function get_profile($user_id){
		$this->db->select('u.*,sj.nm_jabatan');
		$this->db->join('struktur_jabatan sj','sj.id_struktur_jabatan=u.id_jabatan','LEFT');
		$this->db->where('u.id',$user_id);
		return $this->db->get('users u')->row();
	}
	function get_kanwil_uraian(){
		$this->db->select('uraian');
		return $this->db->get('kanwil')->result_array();
	}
	function get_kanwil_jabatan(){
		return $this->db->get('struktur_jabatan_kanwil')->result_array();
	}
	function get_upt_jabatan(){
		return $this->db->get('struktur_jabatan_upt')->result_array();
	}
	function get_jabatan_system($jabatan,$unit){
		$this->db->select('id');
		$this->db->where('id_unit',$jabatan);
		$this->db->where('unit',$unit);
		return $this->db->get('groups')->row();
	}
	function get_id_by_username($username){
		$this->db->select('id,approval_status');
		return $this->db->get_where('users',array('username'=>$username))->row();
	}
	function get_jabatan_title($unit,$jabatan){
		$table = null;
		if($unit == 'ditjenpas'){
			$table = 'struktur_jabatan';
		}else if($unit == 'kanwil'){
			$table = 'struktur_jabatan_kanwil';
		}else{
			$table = 'struktur_jabatan_upt';
		}
		$this->db->select('nm_jabatan');
		$this->db->where('id_struktur_jabatan',$jabatan);
		return $this->db->get($table)->row();
	}
	function id_kanwil_jabatan($kanwil){
		$this->db->select('kode');
		$this->db->where('uraian',$kanwil);
		return $this->db->get('kanwil')->row();
	}
	function update_profile($id_user,$data){
		$this->db->where('id',$id_user);
		$this->db->update('users',$data);
	}
	function set_online($id,$status){
		$this->db->where('id',$id);
		$this->db->set('is_online',$status);
		$this->db->update('users');
	}
	function send_chat($data){
		$this->db->insert('chat',$data);
		return $this->db->insert_id();
	}
	function insert_chat_status($data){
		$this->db->insert('chat_status',$data);
	}
	function check_group_jabatan($jabatan,$unit){
		$this->db->where('unit',$unit);
		$this->db->where('id_jabatan',$jabatan);
		return $this->db->get('group_chat_user')->row();
	}
	function get_id_group($id_unit,$unit){
		$this->db->where('id_unit',$id_unit);
		$this->db->where('unit',$unit);
		return $this->db->get('groups')->row();
	}
	function get_kanwil_group(){
		return $this->db->get('kanwil')->result_array();
	}
	function create_gcu($jabatan,$unit){
		$data = array(
			'category'		=> 'group',
			'status'		=> 1,
			'unit'			=> $unit,
			'id_jabatan'	=> $jabatan,
		);
		$this->db->insert('group_chat_user',$data);
		return $this->db->insert_id();
	}
	function join_gcu($id_gcu,$data){
		$this->db->where('id',$id_gcu);
		$this->db->update('group_chat_user',$data);
	}
	function join_gcu_by_id_group($idGroup,$data){
		$this->db->where('id_group',$idGroup);
		$this->db->update('group_chat_user',$data);
	}
	function get_anggota_grup($id){
		$this->db->where('g.id',$id);
		$this->db->join('groups g','g.id = gcu.id_group');
		return $this->db->get('group_chat_user gcu')->row();
	}
	function get_data_register_by_username($nip){
		return $this->db->get_where('users',array('username'=>$nip))->row();
	}
	function get_gcu_member($idReceiver){
		$this->db->select('gcu.*');
		$this->db->where('gcu.id',$idReceiver);
		return $this->db->get('group_chat_user gcu')->row();
	}
	// function get_chat($groupType,$dest_id,$user_id,$flag){
	function get_chat($dest_id,$user_id,$flag){
		
		$this->db->select('c.*,
							(SELECT concat(first_name,last_name) FROM users WHERE id=c.sender_id) as sender,
							(SELECT concat(first_name,last_name) FROM users WHERE id=c.receiver_id) as receiver,
							cs.flag as flag_user, cs.flag_download, cs.id as id_chat_status
						  ');
		
		if($flag===0 || $flag===1){
			$this->db->where('cs.flag',$flag);
		}
		$this->db->join('chat_status cs','cs.chat_id=c.id');
		
		$this->db->where('c.group_chat_id',$dest_id);
		$this->db->where('cs.user_id',$user_id);
		
		// if($groupType=='personal'){
			// // $this->db->where('cs.user_id',$user_id);
			// // $this->db->where('c.receiver_id',$dest_id);
			// // $this->db->where('c.sender_id',$user_id);
			// // $this->db->where('cs.user_id',$user_id);
		// }else{
			// // $this->db->where('cs.user_id',$user_id);
			// // $this->db->where('c.receiver_id',$dest_id);
			// // $this->db->where('c.sender_id',$user_id);
			// // $this->db->where('cs.user_id',$user_id);			
		// }
		
		$this->db->order_by('c.id','asc');
		return $this->db->get('chat c')->result_array();
	}
	function get_faq(){
		return $this->db->get('faq')->result_array();
	}
	function update_flag($data,$chat_id,$user_id){
		$this->db->where('chat_id',$chat_id);
		$this->db->where('user_id',$user_id);
		$this->db->update('chat_status',$data);
	}
	function download_image($id,$data){
		$this->db->where('id',$id);
		$this->db->update('chat_status',$data);
	}
	function get_direktorat(){
		$this->db->select('id_struktur,nm_struktur');
		return $this->db->get_where('struktur_organisasi',array('parent_id'=>1))->result_array();
	}
	function get_subdirektorat($id){
		$this->db->select('id_struktur,nm_struktur');
		return $this->db->get_where('struktur_organisasi',array('parent_id'=>$id))->result_array();
	}
	function get_jabatan($id){
		$this->db->select('sj.id_struktur_jabatan,so.id_struktur,sj.nm_jabatan');
		$this->db->join('struktur_jabatan sj','sj.fk_struktur_organisasi = so.id_struktur');
		$this->db->where('so.parent_id',$id);
		$this->db->or_where('sj.fk_struktur_organisasi',$id);
		return $this->db->get('struktur_organisasi so')->result_array();
	}
	function get_jabatan_by_parent_id($id){
		$this->db->select('sj.id_struktur_jabatan,so.id_struktur,sj.nm_jabatan');
		$this->db->join('struktur_jabatan sj','sj.fk_struktur_organisasi = so.id_struktur');
		$this->db->where('so.parent_id',$id);
		return $this->db->get('struktur_organisasi so')->result_array();
	}
	function get_upt_by_kanwil($id_kanwil){
		$this->db->where('kanwil',$id_kanwil);
		return $this->db->get('upt')->result_array();
	}
	function get_id_kanwil($kanwil){
		$this->db->select('kode');
		return $this->db->get_where('kanwil',array('uraian'=>$kanwil))->row();
	}
	function update_forgotten_password_code($nip,$data){
		$this->db->where('username',$nip);
		$this->db->update('users',$data);
	}
	function get_user_by_nip_and_kode($nip,$kode){
		$this->db->where('username',$nip);
		$this->db->where('forgotten_password_code',$kode);
		return $this->db->get('users')->row();
	}
	function check_nip($nip){
		$this->db->where('username',$nip);
	}
}


