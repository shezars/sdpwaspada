<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master extends CI_Controller {
	function __construct() {
        parent::__construct();
		if (!$this->ion_auth->logged_in())
		{
		  redirect(base_url('login'));
		}
		$this->load->helper('derumail');
		$this->load->helper('derutext');
		$this->load->model('main_model','main');
    }
	public function index()
	{
		$id_user				= $this->ion_auth->get_user_id();
		$data['group_chat_user']= $this->main->group_chat_user($id_user);
		$data['page']	= 'v_kontak';
		$this->load->view('v_main',$data);
	}
	function pengguna(){
		$id_user				= $this->ion_auth->get_user_id();
		$data['unit_current_user']		= $this->main->get_user($id_user)->unit;
		$data['get_all_user']	= $this->main->get_all_user();
		$data['page']			= 'v_pengguna';
		$this->load->view('v_main',$data);
	}
	function grup(){
		$id_user				= $this->ion_auth->get_user_id();
		$data['unit_current_user']		= $this->main->get_user($id_user)->unit;
		$data['get_all_grup']	= $this->main->get_all_grup();
		$data['page']			= 'v_master_grup';
		$this->load->view('v_main',$data);
	}
	function get_grup(){
		$id			= $this->input->post('id');
		$data		= $this->main->get_grup($id);
		echo json_encode($data);
	}
	function add_grup(){
		$nama_grup 	= $this->input->post('nama_grup');
		$unit 		= $this->input->post('unit_grup');
		$data = array(
			'name'	=> $nama_grup,
			'unit'	=> $unit,
			'type'	=> 'group',
		);
		$this->main->add_grup($data);
		redirect(base_url('master/grup'));
	}
	function get_anggota_grup(){
		$id 				= $this->input->post('id');
		$get_anggota_grup 	= $this->main->get_anggota_grup($id);
		// $tampung=Array();
		if(!empty($get_anggota_grup)){
			$id_anggota_grup = explode(',',$get_anggota_grup->user);
			
			
			foreach($id_anggota_grup as $row){
				$get_user 	= $this->main->get_user($row);
				$user_name 	= $get_user->first_name.$get_user->last_name;
				$pp 		= $get_user->pp;
				$id 		= $get_user->id;
				
				$temp[] = array(
					'id_user'	=> $row,
					'name'		=> $user_name,
					'pp'		=> $pp,
					'id'		=> $id
				);
			}
			
			echo json_encode($temp);
		}else{
			echo 0;
		}
	}
	function get_all_anggota_grup_where(){
		$id_group	= $this->input->post('id');
		
		$data['all_anggota']		= $this->main->get_all_anggota();
		$data['all_anggota_active']	= null;
		
		$get_anggota_grup 	= $this->main->get_anggota_grup($id_group);
		
		if(!empty($get_anggota_grup)){
			$id_anggota_grup = explode(',',$get_anggota_grup->user);
			
			foreach($id_anggota_grup as $row){
				$get_user 	= $this->main->get_user($row);
				$user_name 	= $get_user->first_name.$get_user->last_name;
				$pp 		= $get_user->pp;
				$id 		= $get_user->id;
				
				
				$data['all_anggota_active'][] = array(
					'id_user'	=> $row,
					'name'		=> $user_name,
					'pp'		=> $pp,
					'id'		=> $id
				);
			}
			
			// echo json_encode($temp);
		}else{
			// echo 0;
		}
		
		echo json_encode($data);
	}
	function tambahAnggotaGrup(){
		$idUser 	= $this->input->post('idUser');
		$idGroup 	= $this->input->post('idGroup');
		
		//GET DATA FROM GROUP DETAIL
		$group_data	= $this->main->get_grup($idGroup);
		
		//IF GCU DIDN'T EXISTS THEN CREATED IT
		$check_gcu	= $this->main->check_gcu($idGroup);
		if(empty($check_gcu)){
			//CREATED GCU
			$id_gcu = $this->main->create_gcu($group_data->id_unit,$group_data->unit);
			
			//JOIN GCU
			$data	= array(
						'id_group'	=> $idGroup,
						'user'		=> $idUser
					);
					
			$this->main->join_gcu($id_gcu,$data);
		}else{
			//JOIN GCU
			$users_gcu	= $check_gcu->user;
			$users_gcu	= $users_gcu.','.$idUser;
			$data		= array(
							'id_group'	=> $idGroup,
							'user'		=> $users_gcu
						);
					
			$this->main->join_gcu($check_gcu->id,$data);
		}
		
		// $this->main->
		echo 'kodok= '.$idUser;
	}
	function hapusAnggotaGrup(){
		$idUser 	= $this->input->post('idUser');
		$idGroup 	= $this->input->post('idGroup');
		
		//HAPUS ANGGOTA GRUP PENDING
		$gcu 		= $this->main->check_gcu($idGroup);
		$gcu_user	= explode(',',$gcu->user);
		
		$temp 		= null;
		$jumlah		= count($gcu_user);
		$y 			= 1;
		$z			= 1;
		
		for($x=0;$x<count($gcu_user);$x++){
			if($gcu_user[$x] != $idUser){
				if($z==1){
					$temp .= $gcu_user[$x];
					$z++;
				}
				// else if($x == $jumlah){
					// $temp .= $gcu_user[$x];
				else{
					$temp .= ','.$gcu_user[$x];
				}
				// array_push($temp,$gcu_user[$x]);
			}
			$y++;
		}
		
		$data = array(
			'user'		=> $temp
		);
		
		$this->main->join_gcu_by_id_group($idGroup,$data);
		
		echo 1;
	}
	function edit_grup(){
		$id_grup 	= $this->input->post('id_grup');
		$nama_grup 	= $this->input->post('nama_grup');
		$unit 		= $this->input->post('unit_grup');
		$data = array(
			'name'	=> $nama_grup,
			'unit'	=> $unit,
		);
		
		$this->main->edit_grup($data,$id_grup);
		redirect(base_url('master/grup'));
	}
	function faq(){
		$id_user				= $this->ion_auth->get_user_id();
		$data['unit_current_user']		= $this->main->get_user($id_user)->unit;
		$data['get_all_faq']	= $this->main->get_all_faq();
		$data['page']			= 'v_master_faq';
		$this->load->view('v_main',$data);
	}
	function panduan(){
		$id_user				= $this->ion_auth->get_user_id();
		$data['unit_current_user']		= $this->main->get_user($id_user)->unit;
		$data['get_all_grup']	= $this->main->get_all_grup();
		$data['page']			= 'v_master_panduan';
		$this->load->view('v_main',$data);
	}
	function get_pengguna(){
		echo json_encode($this->main->get_user($this->input->post('id')));
	}
	function get_faq(){
		echo json_encode($this->main->get_faq_detail($this->input->post('id')));
	}
	function ins_faq(){
		$judul		= $this->input->post('judul');
		$deskripsi	= $this->input->post('deskripsi');
		
		$data = array(
			'judul'		=> $judul,
			'deskripsi'	=> $deskripsi,
		);
		
		$this->main->ins_faq($data);
		redirect(base_url('master/faq'));
	}
	function upd_faq(){
		$id			= $this->input->post('id');
		$judul		= $this->input->post('judul');
		$deskripsi	= $this->input->post('deskripsi');
		
		$data = array(
			'judul'	=> $judul,
			'deskripsi'	=> $deskripsi,
		);
		
		$this->main->upd_faq($id,$data);
		redirect(base_url('master/faq'));
	}
	function del_faq(){
		$id = $this->input->post('id');
		$this->main->del_faq($id);
	}
	function upd_pengguna(){
		$id 			= $this->input->post('id');
		$state 			= $this->input->post('state');
		
		$data			= null;
		
		if($state=='true'){
			$dataPersonal	= $this->main->get_user($id);
			$data = array(
				'level'			=> 1
			);
		}else{
			$dataPersonal	= $this->main->get_user($id);
			$data = array(
				'level'			=> 0
			);
		}
		$this->main->upd_pengguna($data,$id);
	}
	function approve_pengguna($param){
		$id 		= $this->input->post('id');
		$id_user	= $this->ion_auth->get_user_id();
		
		if($param == 'tolak'){
			$data = array(
				'approval_pic'		=> $id_user,
				'approval_time'		=> date('Y-m-d'),
				'approval_status'	=> 2,
				'is_deleted'		=> 1,
			);
		}else{
			$data = array(
				'approval_pic'		=> $id_user,
				'approval_time'		=> date('Y-m-d'),
				'approval_status'	=> 1
			);
		}
		
		$this->db->where('id',$id);
		$this->db->update('users',$data);
		
		$profile = $this->main->get_user($id);
		$data['nama_lengkap']	= $profile->first_name.''.$profile->last_name;
		
		if($param == 'tolak'){
			//Send SMS Notifikasi
			send_sms($profile->no_hp,'Mohon Maaf Akun Anda Tidak Disetujui');
			
			//Send Email Notifikasi
			$template	= $this->load->view('template_tolak_approval',$data,true);
			send($profile->email,'Approval SDP Waspada '.$profile->username,$template);
		}else{
			//Send SMS Notifikasi
			send_sms($profile->no_hp,'Selamat Akun Anda Sudah Berhasil Disetujui');
			
			//Send Email Notifikasi
			$template	= $this->load->view('template_sukses_approval',$data,true);
			send($profile->email,'Approval SDP Waspada '.$profile->username,$template);
		}
		
	}
}
