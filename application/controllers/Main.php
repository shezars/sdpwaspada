<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {
	function __construct() {
        parent::__construct();
		if (!$this->ion_auth->logged_in())
		{
			redirect(base_url('login'));
		}
		$this->load->model('main_model','main');
    }
	public function index()
	{
		$id_user				= $this->ion_auth->get_user_id();
		$data['group_chat_user']= $this->main->group_chat_user($id_user);
		$data['page']			= 'v_chat';
		$this->load->view('v_main',$data);
	}
	function update_token(){
		$username = $this->input->post('username');
		$token = $this->input->post('token');
		$this->db->query("UPDATE users SET token='$token' WHERE username='$username'");
	}
	function kodok(){
		$this->load->library('Compress');  // load the codeginiter library
		
		//File to block
		// $file0 = base_url().'assets/img/snake_river.jpg'; // file that you wanna compress
		$file0 = base_url().'kodok/132.png'; // file that you wanna compress
		$new_name_image0 = 'issue_resized22'; // name of new file compressed
		$quality0 = 0.5; // Value that I chose
		$destination0 = base_url().'kodok'; // This destination must be exist on your project
		
		$compress0 = new Compress();
		
		$compress0->file_url = $file0;
		$compress0->new_name_image = $new_name_image0;
		$compress0->quality = $quality0;
		$compress0->destination = $destination0;
		
		$result0 = $compress0->compress_image();
		echo '<pre>';
		var_dump($result0);
	}
	function kodok2(){
		$this->load->library('image_lib');
		
		// $config['source_image'] 	= base_url().'kodok/issue_resized.jpg';
		// $config['image_library'] = 'gd';
		$config['source_image'] 	= './kodok/issue_resized.jpg';
		$config['wm_type'] = 'overlay';
		$config['wm_overlay_path'] = './kodok/overlay2.jpg';
		$config['wm_opacity'] = '50';
		// $config['quality'] = 50;	
		$config['wm_vrt_alignment'] = 'middle';
		$config['wm_hor_alignment'] = 'center';
		
		
		// $this->load->library('image_lib', $config);
		$this->image_lib->initialize($config);
		$status = $this->image_lib->watermark();
		if(!$status){
			echo $this->image_lib->display_errors();
		}else{
			echo 1;
		}
		// print_r($status);
	}
	function send_chat(){
		$sender_id	= $this->ion_auth->get_user_id();
		$sender_name = $this->main->get_user($sender_id)->first_name.' '.$this->main->get_user($sender_id)->last_name;
		$idReceiver	= $this->input->post('idReceiver');
		// $groupType	= $this->input->post('groupType');
		$msg		= $this->input->post('msg');
		
		$data		= array(
						  'sender_id'		=> $sender_id,
						  'msg'				=> $msg,
						  'msg_type'		=> 'text',
						  'created_at'		=> date('Y-m-d H:m:s'),
						  'receiver_id'		=> $idReceiver,
						  'group_chat_id'	=> $idReceiver,
						  // 'receiver_type'	=> $groupType,
						  // 'group_chat_id'	=> $idGroup
					  );
		$chat_id				= $this->main->send_chat($data);
		
		// if($groupType=='personal'){
			// $data = array(
					// 'chat_id'		=> $chat_id,
					// 'user_id'		=> $idReceiver,
					// 'flag'			=> 0,
					// 'created_time'	=> date('Y-m-d H:m:s')
				// );
				// $this->main->insert_chat_status($data);
		// }else{
			
			// $anggota_grup 			= $this->ion_auth->users($idReceiver)->result_array();
			
			$anggota_grup 			= $this->main->get_gcu_member($idReceiver);
			$anggota_grup			= explode(",",$anggota_grup->user);
			$jumlah_anggota_grup 	= count($anggota_grup);
			for($x=0;$x<$jumlah_anggota_grup;$x++){
				$token				= $this->main->get_user($anggota_grup[$x])->token;
				
				$data = array(
					'chat_id'		=> $chat_id,
					'user_id'		=> $anggota_grup[$x],
					'flag'			=> 0,
					'created_time'	=> date('Y-m-d H:m:s')
				);
				
				$this->main->insert_chat_status($data);
				
				//Send Notifications
				$this->send_notification($token,$msg,$sender_name);
			}
			// foreach($anggota_grup as $row){
				
			// }
		
		// }
		
	}
	function get_chat(){
		$dest_id 	= $this->input->post('id');
		// $groupType 	= $this->input->post('groupType');
		$user_id	= $this->ion_auth->get_user_id();
		
		// $data_chat 	= $this->main->get_chat($groupType,$dest_id,$user_id,'all');
		$data_chat 	= $this->main->get_chat($dest_id,$user_id,'all');
		
		if(count($data_chat)>1){
			$data_chat	= array(
				'data_chat'		=> $data_chat,
				'receiver_id'	=> $user_id,
				'jumlah'		=> count($data_chat)
			);
			echo json_encode($data_chat);
		}else{
			if(empty($data_chat)){
				$data_chat	= array(
					'data_chat'		=> '',
					'receiver_id'	=> $user_id,
					'jumlah'		=> count($data_chat)
				);
			}else{
				$data_chat	= array(
					'data_chat'		=> $data_chat[0],
					'receiver_id'	=> $user_id,
					'jumlah'		=> count($data_chat)
				);
			}
			
			echo json_encode($data_chat);
		}	
	}
	function get_chat_specific(){
		$dest_id 	= $this->input->post('id');
		$user_id	= $this->ion_auth->get_user_id();
		
		$data_chat 	= $this->main->get_chat($dest_id,$user_id,0);
		
		if(count($data_chat)>1){
			$data_chat	= array(
				'data_chat'		=> $data_chat,
				'receiver_id'	=> $user_id,
				'jumlah'		=> count($data_chat)
			);
			echo json_encode($data_chat);
		}else{
			if(empty($data_chat)){
				$data_chat	= array(
					'data_chat'		=> '',
					'receiver_id'	=> $user_id,
					'jumlah'		=> count($data_chat)
				);
			}else{
				$data_chat	= array(
					'data_chat'		=> $data_chat[0],
					'receiver_id'	=> $user_id,
					'jumlah'		=> count($data_chat)
				);
			}
			
			echo json_encode($data_chat);
		}	
	}
	function update_flag(){
		$chat_id = $this->input->post('id');
		$user_id = $this->ion_auth->get_user_id();
		
		$data['flag']			= 1;
		$data['updated_time']	= date('Y-m-d H:m:s');
		$this->main->update_flag($data,$chat_id,$user_id);
	}
	function send_attachment(){
		
		$this->load->library('Compress');
		
		$file 							= $_FILES['file'];
		$type_file						= explode('/',$file['type']);
		$type_file						= $type_file[0];
		$thumbnail_pic					= null;
		
		if($type_file=='image'){
			$config['upload_path']  	= './uploads/images/real';
		}else if($type_file=='video'){
			$config['upload_path']  	= './uploads/videos/';
		}else{
			$config['upload_path']   	= './uploads/documents/';
		}
		
		$sender_id						= $this->ion_auth->get_user_id();
		$idReceiver						= $this->input->post('idReceiver');
		
		// $config['allowed_types']        = 'gif|jpg|png';
		$config['allowed_types']        = '*';
		$config['max_size']             = 20000;
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;
		$this->load->library('upload', $config);
		
		if ( ! $this->upload->do_upload('file'))
		{
			$error = array('error' => $this->upload->display_errors());
		}
		else
		{
			$dataFile 	= $this->upload->data();
			$filename	= $dataFile['file_name'];
			$rawname	= $dataFile['raw_name'];
			
			
			if($type_file=='image'){
				//Compress Gambar Dan Update Ke Table
				$file 						= base_url().'uploads/images/real/'.$dataFile['file_name']; 
				$new_name_image 			= $filename;
				$quality 					= 0.5;
				$destination 				= base_url().'uploads/images/compress';
				$compress 					= new Compress();
				
				$compress->file_url			= $file;
				$compress->new_name_image 	= $new_name_image;
				$compress->quality 			= $quality;
				$compress->destination 		= $destination;
				$compress->compress_image();
				
			}else if($type_file == 'video'){
				$lokasi_video 	= base_url().'uploads/videos/'.$filename;
				$lokasi_thumb 	= './uploads/videos/thumbnail/'.$rawname.'.jpg';
				$thumbnail_pic	= $rawname.'.jpg';
				exec('ffmpeg -y -itsoffset -2 -i "'.$lokasi_video.'" -vcodec mjpeg -vframes 1 -an -f rawvideo -s 300x300 '.$lokasi_thumb.'');
			}else{
				
			}
			
			$data	= array(
						'sender_id'		=> $sender_id,
						'receiver_id'	=> $idReceiver,
						'msg'			=> $dataFile['file_name'],
						'msg_type'		=> $type_file,
						'size'			=> round($dataFile['file_size']),
						'thumbnail_pic'	=> $thumbnail_pic,
						'created_at'	=> date('Y-m-d H:m:s'),
						'group_chat_id'	=> $idReceiver
					);
			
			$chat_id				= $this->main->send_chat($data);
			
			// $anggota_grup 			= $this->ion_auth->users($idGroup)->result_array();
			// $jumlah_anggota_grup 	= count($anggota_grup);
			// foreach($anggota_grup as $row){
				// $data = array(
					// 'chat_id'		=> $chat_id,
					// 'user_id'		=> $row['user_id'],
					// 'flag'			=> 0,
					// 'flag_download'	=> 0,
				// );
				// $this->main->insert_chat_status($data);
			// }
			
			$anggota_grup 			= $this->main->get_gcu_member($idReceiver);
			$anggota_grup			= explode(",",$anggota_grup->user);
			$jumlah_anggota_grup 	= count($anggota_grup);
			for($x=0;$x<$jumlah_anggota_grup;$x++){
				$data = array(
					'chat_id'		=> $chat_id,
					'user_id'		=> $anggota_grup[$x],
					'flag'			=> 0,
					'created_time'	=> date('Y-m-d H:m:s')
				);
				$this->main->insert_chat_status($data);
			}
		}
	}
	function download_image(){
		$id 		= $this->input->post('id');
		$data = array(
			'flag_download'	=> 1,
		);
		$this->main->download_image($id,$data);
	}
	function send_notification($token,$body,$sender_name){
		$url = "https://fcm.googleapis.com/fcm/send";
		$token = $token;
		$serverKey = 'AAAADAOP1dc:APA91bFQWOxa2TpKxIF7aK-pcARVUZlQUK2iZ2Bjfl-PfR0RqMsscypOQRwkXpk9qjU8IyR-4XYYP2ZFvV1AA7O9svhYhjNV-iUyBXs3PgW2PhZQ8vOs2aRbCQHY8p0rI04ERU2sKTEw';
		$title = $sender_name;
		$notification = array('title' =>$title , 'body' => $body, 'sound' => 'default', 'badge' => '1');
		$arrayToSend = array('to' => $token, 'notification' => $notification,'priority'=>'high');
		$json = json_encode($arrayToSend);
		$headers = array();
		$headers[] = 'Content-Type: application/json';
		$headers[] = 'Authorization: key='. $serverKey;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
		curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
		//Send the request
		$response = curl_exec($ch);
		//Close request
		if ($response === FALSE) {
		die('FCM Send Error: ' . curl_error($ch));
		}
		curl_close($ch);
	}
	
}
