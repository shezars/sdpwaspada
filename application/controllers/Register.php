<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {
	function __construct() {
        parent::__construct();
		$this->load->helper('derumail');
		$this->load->helper('derutext');
		$this->load->model('main_model','main');
    }
	public function index()
	{
		$data['direktorat']		= $this->main->get_direktorat();
		$data['kanwil_jabatan']	= $this->main->get_kanwil_jabatan();
		$data['upt_jabatan']	= $this->main->get_upt_jabatan();
		$this->load->view('v_register',$data);
	}
	function register_plain(){
		$data['direktorat']		= $this->main->get_direktorat();
		$data['kanwil_jabatan']	= $this->main->get_kanwil_jabatan();
		$data['upt_jabatan']	= $this->main->get_upt_jabatan();
		$this->load->view('v_register_plain',$data);
	}
	function get_kanwil_uraian(){
		$kanwil_uraian			= $this->main->get_kanwil_uraian();
		$contoh = Array();
		foreach($kanwil_uraian as $row){
			array_push($contoh,$row['uraian']);
		}
		
		header('Content-Type: application/json');
		echo json_encode($contoh);
	}
	function get_upt_by_kanwil(){
		$kanwil 	= $this->input->post('kanwil');
		$id_kanwil	= $this->main->get_id_kanwil($kanwil);
		echo json_encode($this->main->get_upt_by_kanwil($id_kanwil->kode));
	}
	function get_subdirektorat(){
		$id = $this->input->post('id');
		echo json_encode($this->main->get_subdirektorat($id));
	}
	function get_jabatan(){
		$id 	 = $this->input->post('id');
		$result  = $this->main->get_jabatan($id);
		$result2 = $result;
		$count 	 = count($result2);
		$count 	 = $count++;
		
		$temp = null;
		
		//Add Child 1 Level More
		foreach($result as $key => $row){
			if($row['id_struktur']!=$id){
				$temp_result = $this->main->get_jabatan_by_parent_id($row['id_struktur']);
				if(count($temp_result)==1){
					$result2[$count] = $temp_result[0];
				}else{
					foreach($temp_result as $roww){
						$result2[$count] = $roww;
					}
				}
				
				$count++;
			}
			
		}
		
		// array_push($result2,$temp);
		// $result2[] = $temp;
		// print_r($result2);
		echo json_encode($result2);
	}
	function do_register(){
		
		$nama_lengkap 			= $this->input->post('nama_lengkap');
		$jekel 					= $this->input->post('jekel');
		$nip 					= $this->input->post('nip');
		$no_hp 					= $this->input->post('no_hp');
		$email 					= $this->input->post('email');
		$password 				= $this->input->post('password');
		$unit 					= $this->input->post('unit');
		
		$username 				= $nip;
		$password 				= $password;
		$email 					= $email;
		
		$additional_data = array(
					'first_name'			=> $nama_lengkap,
					'jekel' 				=> $jekel,
					'no_hp' 				=> $no_hp,
					'unit' 					=> $unit,
					);
		
		$jabatan 									= $this->input->post('jabatan');
		$additional_data['id_jabatan']				= $jabatan;
		
		$jabatan_system								= $this->main->get_jabatan_system($jabatan,$unit);
		$jabatan_system								= $jabatan_system->id;
		
		$jabatan_title								= null;
		$id_upt										= null;
		if($unit == "ditjenpas"){
			$satker_direktorat 						= $this->input->post('direktorat');
			$satker_sub_direktorat 					= $this->input->post('sub_direktorat');
			$additional_data['satker_direktorat']	= $satker_direktorat;
			$additional_data['satker_subdirektorat']= $satker_sub_direktorat;
			$jabatan_title							= $this->main->get_jabatan_title('ditjenpas',$jabatan);
		}else if($unit == "kanwil"){
			$kanwil 								= $this->input->post('kanwil');
			$additional_data['kanwil']				= $kanwil;
			$jabatan_title							= $this->main->get_jabatan_title('kanwil',$jabatan);
			$id_kanwil_jabatan						= $this->main->id_kanwil_jabatan($kanwil)->kode;
		}else{
			$kanwil 								= $this->input->post('kanwil');
			$id_kanwil_jabatan						= $this->main->id_kanwil_jabatan($kanwil)->kode;
			$upt				 					= $this->input->post('upt');
			$id_upt									= $upt;
			$additional_data['kanwil']		 		= $kanwil;
			$additional_data['id_upt']				= $upt;
			$jabatan_title							= $this->main->get_jabatan_title('upt',$jabatan);
		}
		
		$group = array($jabatan_system); 
		$this->ion_auth->register($username, $password, $email, $additional_data, $group);
		
		//Get Data Register
		$data_register 			= $this->main->get_data_register_by_username($nip);
		$id_data_register		= $data_register->id;
		
		// //SELURUH PENGGUNA DI MAPPING KE USER ADMIN
		// $data_get_all_tanpa_admin = $this->db->query("SELECT id,approval_status,first_name,last_name FROM users WHERE id <> 1")->result_array(); 
		
		if($unit == "ditjenpas"){
			//JIKA UNIT = DITJENPAS => MASUKKAN KEDALAM SELURUH KANWIL
			$group_default			= $this->main->get_kanwil_group();
			foreach($group_default as $row){
				$check_group_jabatan	= $this->main->check_group_jabatan($row['kode'],$unit);
				$get_id_group			= $this->main->get_id_group($row['kode'],"default")->id;
				
				if(empty($check_group_jabatan)){
					//Create New GCU
					$id_gcu = $this->main->create_gcu($row['kode'],$unit);
					//Join To First Data GCU
					$data = array(
						'id_group'	=> $get_id_group,
						'user'		=> $id_data_register
					);
					$this->main->join_gcu($id_gcu,$data);
				}else{
					//Join To Next Data GCU
					$id_gcu 	= $check_group_jabatan->id;
					$users_gcu	= $check_group_jabatan->user;
					$users_gcu	= $users_gcu.','.$id_data_register;
					$data		= array(
						'id_group'	=> $get_id_group,
						'user'		=> $users_gcu
					);
					$this->main->join_gcu($id_gcu,$data);
				}
			}
			
			//TAMBAH SELURUH PERSONAL DIBAWAH DITJENPAS
			$data_users_ditjenpas = $this->db->query("SELECT id,approval_status,first_name,last_name FROM users WHERE unit = 'ditjenpas' AND id <> ".$id_data_register." OR id = 1")->result_array(); 
			
			//KARENA PENGGUNA BARU LANGSUNG DI MASUKAN KEDALAM GCU PERSONAL
			foreach($data_users_ditjenpas as $row){	
				$data = array(
					'category'	=> 'personal',
					'status'	=> 1,
					'unit'		=> $unit,
					'user'		=> $id_data_register.','.$row['id']
				);
				$this->main->insert_gcu_personal($data);
			}
			
		}else{
			//JIKA UNIT = UPT => MASUKKAN KEDALAM KANWIL UPT TERSEBUT
			$check_group_jabatan	= $this->main->check_group_jabatan($id_kanwil_jabatan,$unit);
			if(empty($check_group_jabatan)){
				//Create New GCU
				$id_gcu 			= $this->main->create_gcu($id_kanwil_jabatan,$unit);
				$get_id_group		= $this->main->get_id_group($row['kode'],$unit)->id;
				
				//Join To First Data GCU
				$data = array(
					'id_group'	=> $get_id_group,
					'user'		=> $id_data_register
				);
				$this->main->join_gcu($id_gcu,$data);
			}else{
				//Join To Next Data GCU
				$id_gcu 	= $check_group_jabatan->id;
				$users_gcu	= $check_group_jabatan->user;
				$users_gcu	= $users_gcu.','.$id_data_register;
				$data		= array(
					'id_group'	=> $get_id_group,
					'user'		=> $users_gcu,
				);
				$this->main->join_gcu($id_gcu,$data);
			}
			
			//CARI MEMBER DALAM KANWIL YG SAMA
			$data_member_kanwil = $this->db->query("SELECT id,approval_status,first_name,last_name FROM users WHERE unit = 'kanwil' AND id <> ".$id_data_register." OR id = 1")->result_array(); 
			
			//KARENA PENGGUNA BARU LANGSUNG DI MASUKAN KEDALAM GCU PERSONAL
			foreach($data_member_kanwil as $row){
				$data = array(
					'category'	=> 'personal',
					'status'	=> 1,
					'unit'		=> $unit,
					'id_upt'	=> $id_upt,
					'user'		=> $id_data_register.','.$row['id']
				);
				$this->main->insert_gcu_personal($data);
			}
		}
		
		
		$data['nama_lengkap']	= $nama_lengkap;
		$data['jabatan']		= $jabatan_title->nm_jabatan;
		$data['username']		= $nip;
		$data['password']		= $password;
		$data['status']			= 'Registrasi Berhasil, Menunggu Approval Pimpinan';
		
		//Send SMS Notifikasi
		// send_sms($no_hp,'Terima Kasih telah melakukan registrasi, mohon menunggu proses approval');
		
		//Send Email Notifikasi
		$template	= $this->load->view('template_sukses_registrasi',$data,true);
		send($email,'Registrasi SDP Waspada '.$nip,$template);
		
		echo 1;
	}
	
	function forgot_password(){
		$data['direktorat']		= $this->main->get_direktorat();
		$data['kanwil_jabatan']	= $this->main->get_kanwil_jabatan();
		$data['upt_jabatan']	= $this->main->get_upt_jabatan();
		$this->load->view('v_forgot_password',$data);
	}
	function get_user_by_nip(){
		$nip 	= $this->input->post('nip');
		$user 	= $this->main->get_user_by_nip($nip);
		
		$result = null;
		
		if(empty($user)){
			$result = array(
				'status'	=> 0,
				'data'		=> null
			);
		}else{
			//Create 4 Digit Verification
			$digit = rand(1000,9999);
			
			//Update Data
			$data = array(
				'forgotten_password_code'	=> $digit
			);
			$this->main->update_forgotten_password_code($nip,$data);
			
			//Send Email Notifikasi
			$data['digit']= $digit;
			$template	= $this->load->view('template_forgot_password',$data,true);
			$email		= $user->email;
			
			send($email,'SDP Waspada Reset Password',$template);
			
			
			$result = array(
				'status'	=> 1,
				'data'		=> $user
			);
		}
		
		echo json_encode($result);
	}
	function verifikasi_reset_password(){
		$nip 	= $this->input->post('nip');
		$kode 	= $this->input->post('kode');
		$result = null;
		
		$hasil = $this->main->get_user_by_nip_and_kode($nip,$kode);
		
		if(empty($hasil)){
			$result = array(
				'status'	=> 0,
				'data'		=> null
			);
		}else{
			$result = array(
				'status'	=> 1,
				'data'		=> $hasil
			);
		}
		
		echo json_encode($result);
	}
	function proses_reset_password(){
		$idUser 	= $this->input->post('idUser');
		$nip 		= $this->input->post('nip');
		$user 		= $this->main->get_user_by_nip($nip);
		$password 	= $this->input->post('password');
		
		$data = array(
			'password' => $password
		);
		
		$this->ion_auth->update($idUser, $data);
		
		
		//SEND EMAIL NOTIFICATION BERHASIL RESET PASSWORD
		$template	= $this->load->view('template_sukses_reset_password',$data,true);
		$email		= $user->email;
		send($email,'SDP Waspada Reset Password Sukses',$template);
			
		echo 1;
	}
	function check_nip(){
		$nip 	= $this->input->post('nip');
		$data	= $this->main->get_user_by_nip($nip);
		if(empty($data)){
			echo 0;
		}else{
			echo 1;
		}
	}
	
}
