<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	function __construct() {
        parent::__construct();
		$this->load->model('main_model','main');
    }
	public function index()
	{
		$this->load->view('v_login');
	}
	function do_login(){
		// $ua = strtolower($_SERVER['HTTP_USER_AGENT']);
		// if(stripos($ua,'android') !== false) { // && stripos($ua,'mobile') !== false) {
			// echo "your message";
		// }
		// die;
		$username 	= $this->input->post('username');
		$password 	= $this->input->post('password');
		$remember 	= TRUE;
		$data_get	= $this->main->get_id_by_username($username);
		$get_id		= null;
		
		if(empty($data_get)){
			$get_id	= '';
		}else{
			$get_id	= $data_get->id;
		}
		
		if(!empty($get_id)){
			if($this->ion_auth->hash_password_db($get_id, $password) && $data_get->approval_status == 0){
				$info = Array(
					'status'	=> 99,
					'info'		=> 'Pengguna Belum Di Approve, Mohon Menunggu Proses Approval!',
				);
				echo json_encode($info);
				die;
			}
		}
		$hasil 	 	= $this->ion_auth->login($username, $password, $remember);
		$this->main->set_online($this->ion_auth->get_user_id(),1);
		if($hasil){
			
		}else{
			$info = Array(
					'status'	=> 88,
					'info'		=> 'Username atau Password anda Tidak Sesuai!',
				);
			echo json_encode($info);
			die;
		}
		// echo "<script language=\"javascript\">Android.showToast('Indonesia Raya OKE');</script>";
		// sleep(1);
		$info = Array(
					'status'	=> 1,
					'info'		=> '',
				);
		echo json_encode($info);
	}
	
	function logout(){
		// die;
		// if($this->ion_auth->logged_in())
		// {
			// echo $this->ion_auth->logged_in();die;
			// $this->main->set_online($this->ion_auth->get_user_id(),0);
			$this->ion_auth->logout();
			redirect(base_url());
		// }else{
			// echo 'kodok';die;
		// }
	}
}
