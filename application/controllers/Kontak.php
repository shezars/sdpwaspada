<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kontak extends CI_Controller {
	function __construct() {
        parent::__construct();
		if (!$this->ion_auth->logged_in())
		{
		  redirect(base_url('login'));
		}
		$this->load->model('main_model','main');
    }
	public function index()
	{
		$id_user					= $this->ion_auth->get_user_id();
		$data['level']				= $this->main->get_user($id_user)->level;
		$data['group_chat_user']	= $this->main->group_chat_user_kontak($id_user);
		$data['group_chat_user2']	= $this->main->group_chat_user_kontak($id_user,'administrator2');
		$data['page']				= 'v_kontak';
		$this->load->view('v_main',$data);
	}
}
