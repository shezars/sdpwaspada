<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faq extends CI_Controller {
	function __construct() {
        parent::__construct();
		if (!$this->ion_auth->logged_in())
		{
		  redirect(base_url('login'));
		}
		$this->load->model('main_model','main');
    }
	public function index()
	{
		$data['page']	= 'v_faq';
		$data['faq']	= $this->main->get_faq();
		$this->load->view('v_main',$data);
	}
}
