<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil extends CI_Controller {
	function __construct() {
        parent::__construct();
		if (!$this->ion_auth->logged_in())
		{
		  redirect(base_url('login'));
		}
		$this->load->model('main_model','main');
    }
	public function index()
	{
		$data['direktorat']		= $this->main->get_direktorat();
		$data['kanwil_jabatan']	= $this->main->get_kanwil_jabatan();
		$data['upt_jabatan']	= $this->main->get_upt_jabatan();
		
		$data['page']	= 'v_profil';
		$data['data']	= $this->main->get_profile($this->ion_auth->get_user_id());
		$this->load->view('v_main',$data);
	}
	function update_photo(){
		$id_user						= $this->ion_auth->get_user_id();
		
		$config['upload_path']          = './uploads/images/pp';
		$config['allowed_types']        = 'gif|jpg|jpeg|png';
		$config['file_name'] 			= $id_user;
		// $config['max_size']             = 100;
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;

		$this->load->library('upload', $config);
		$this->upload->overwrite = true;
		if (!$this->upload->do_upload('foto'))
		{
			// print_r($this->upload->display_errors());
			echo 0;
		}
		else
		{
			$data = $this->upload->data();
			$this->main->update_pp($id_user,$data['file_name']);
			echo $data['file_name'];
		}	
	}
	function update_profile(){
		$id_user		= $this->ion_auth->get_user_id();
		$namalengkap 	= $this->input->post('namalengkap');
		$telepon 		= $this->input->post('telepon');
		$data = Array(
			'first_name'	=> $namalengkap,
			'no_hp'			=> $telepon,
		);
		$this->main->update_profile($id_user,$data);
	}
	function get_user(){
		$id_user		= $this->ion_auth->get_user_id();
		echo json_encode($this->main->get_profile($id_user));
	}
	function update_password(){
		$oldPassword		= $this->input->post('oldPassword');
		$newPassword		= $this->input->post('newPassword');
		$id_user			= $this->ion_auth->get_user_id();
		$password_matches	= $this->ion_auth->hash_password_db($id_user, $oldPassword);
		if($password_matches){
			$data = Array(
				'password'	=> $newPassword
			);
			$this->ion_auth->update($id_user, $data);
			echo 1;
		}else{
			echo 99;
		}
		
	}
}
