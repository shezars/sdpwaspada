<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master extends CI_Controller {
	function __construct() {
        parent::__construct();
		if (!$this->ion_auth->logged_in())
		{
		  redirect(base_url('login'));
		}
    }
	public function index()
	{
		$data['page']	= 'v_panduan';
		$this->load->view('v_main',$data);
	}
	function pengguna(){
		$data['page']	= 'v_pengguna';
		$this->load->view('v_main',$data);
	}
}
