<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Api extends REST_Controller {
	function __construct() {
        parent::__construct();
		$this->load->model('main_model','main');
    }
	public function login_post()
	{
		$nip	= $this->post('nip');
		$pass   = $this->post('pass');
		
		$username 	= $nip;
		$password 	= $pass;
		
		$data_get	= $this->main->get_id_by_username($username);
		$get_id		= null;
		
		if(empty($data_get)){
			$get_id	= '';
		}else{
			$get_id	= $data_get->id;
		}
		
		if(!empty($get_id)){
			if($this->ion_auth->hash_password_db($get_id, $password) && $data_get->approval_status == 0){
				$info = Array(
					'status'	=> 99,
					'info'		=> 'Pengguna Belum Di Approve, Mohon Menunggu Proses Approval!',
				);
				$this->response($info, REST_Controller::HTTP_OK);
				die;
			}
		}
		
		$hasil 	 	= $this->ion_auth->login($username, $password);
		
		if($hasil){
			$data = $this->db->query("SELECT id as id_user, username as nip, email, concat(first_name,last_name) as name, jekel as gender, no_hp as phone, unit, satker_direktorat, satker_subdirektorat, id_jabatan, kanwil, id_upt, approval_status FROM USERS WHERE id=".$get_id."")->row();
			$info = Array(
					'status'	=> 10,
					'info'		=> 'Sukses Login',
					'data'		=> $data
				);
		}else{
			$info = Array(
					'status'	=> 88,
					'info'		=> 'Username atau Password anda Tidak Sesuai!',
				);
		}
		
		$this->response($info, REST_Controller::HTTP_OK);
	}
	
}
